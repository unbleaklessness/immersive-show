#ifndef HUMAN_H
#define HUMAN_H
#include <QString>
#include <QVector>
#include <QHash>


struct Human
{
    // First Column
    QString name;                   // MEMBER   |   NAME
    QString familyStatus;           // MEMBER   |   FAMILY STATUS
    QString work;                   // MEMBER   |   WORK
    QString location;               // MEMBER   |   LOCATION
    QString relationship;           // MEMBER   |   getFamilyRelationShip
    QVector<QString> familyMembersNames;

    // Second Column
    QHash<QString, QString> relationsInfo;  // Name - Description

    // Third column
    QString organizationDescription;
    QString morning;
    QString day;                    // MEMBER
    QString evening;                // MEMBER

    // Index
    bool sexIndex;                  // MEMBER     | SEX
    int relationshipIndex;          // ROLE       | Relation Ship
    int  organizationIndex;         // ROLE|      | Organization




};
#endif // HUMAN_H
