#include "printer.h"

#include <QDir>

Printer::Printer()
{
    setupPrinter();


    // Setup Columns
    setupFirstColumn();
    setupSecondColumn();
    setupThirdColumn();

    // Setup Fonts
    roboFont = new QFont(":/Fonts/RobotoCondensed/RobotoCondensed.ttf", 13);
    roboFontBold = new QFont(":/Fonts/Roboto/Roboto-Bold.ttf");
}

// Setup
void Printer::setupPrinter()
{
    printer = new QPrinter;
    printer->setOutputFormat(QPrinter::PdfFormat);
    printer->setOutputFileName(QDir::currentPath() + "/layout.pdf");
    printer->setPageOrientation(QPageLayout::Landscape);
    printer->setResolution(300);
    printer->setPaperSize(QPrinter::A4);
    printer->setFullPage(true);
    printer->setPageMargins(QMarginsF(0,0,0,0));
}

void Printer::addPage(Human human)
{

    printer->newPage();
    // DRAW TEXT
    //=========================================================

    // Static Text
    roboFontBold->setPointSize(18);
    roboFontBold->setBold(true);
    painter->setFont(*roboFontBold);

    // First column
    painter->drawText(*firstColumn[4], Qt::AlignRight | Qt::TextWordWrap, "СЕМЬЯ");

    // Second column
    painter->drawText(*secondColumn[0], Qt::AlignLeft | Qt::TextWordWrap, "О КОМ ТЫ ДУМАЕШЬ?");

    // Third column
    painter->drawText(*thirdColumn[0], Qt::AlignLeft, "ДЕЯТЕЛЬНОСТЬ");
    painter->drawText(*thirdColumn[2], Qt::AlignLeft, "РАСПОРЯДОК ДНЯ");

    //#########################################################
    // Dynamic Text
    // First column
    //
    roboFontBold->setPointSize(20);
    painter->setFont(*roboFontBold);
    //
    painter->drawText(*firstColumn[0], Qt::AlignLeft | Qt::TextWordWrap, human.name);
    //
    roboFont->setPointSize(13);
    painter->setFont(*roboFont);
    //
    painter->drawText(*firstColumn[1], Qt::AlignRight | Qt::TextWordWrap, human.familyStatus);
    painter->drawText(*firstColumn[2], Qt::AlignRight | Qt::TextWordWrap, human.work);
    painter->drawText(*firstColumn[3], Qt::AlignRight | Qt::TextWordWrap, human.location);
    // Family
    int i = 0;
    foreach (QString name, human.familyMembersNames)
        if (human.name != name)
        {
            painter->drawText(*firstColumn[i + 5], Qt::AlignRight | Qt::TextWordWrap, name);
            i++;
        }
    painter->drawText(*firstColumn[9], Qt::AlignRight | Qt::TextWordWrap, human.relationship);
    drawRectSeparators(1080, 8);

    // Third Column
    painter->drawText(*thirdColumn[1], Qt::AlignLeft | Qt::TextWordWrap, human.organizationDescription);
    painter->drawText(*thirdColumn[3], Qt::AlignLeft | Qt::TextWordWrap, human.morning);
    painter->drawText(*thirdColumn[4], Qt::AlignLeft | Qt::TextWordWrap, human.day);
    painter->drawText(*thirdColumn[5], Qt::AlignLeft | Qt::TextWordWrap, human.evening);
    drawRectSeparators(2400, 8);

    // Second Column
    if (human.relationsInfo.keys().length() == 0) return;

    QList<QString> descriptions = human.relationsInfo.values();
    int descriptionIndex = 0;
    QString biggestDescription = "Empty";
    for (int i = 0; i < descriptions.length(); i++)
        if (descriptions[i].length() > biggestDescription.length())
        {
            biggestDescription = descriptions[i],
            descriptionIndex = i;
        }
    painter->drawText(*secondColumn[2], Qt::AlignLeft | Qt::TextWordWrap, human.relationsInfo.values()[descriptionIndex]);
    QString nameOfBiggest = human.relationsInfo.keys()[descriptionIndex];
    human.relationsInfo.remove(nameOfBiggest);
    descriptions.removeAt(descriptionIndex);


    roboFontBold->setPointSize(13);
    painter->setFont(*roboFontBold);

    painter->drawText(*secondColumn[1], Qt::AlignLeft | Qt::TextWordWrap, nameOfBiggest);
    for (int i = 0; i < human.relationsInfo.keys().length(); i++)
        painter->drawText(*secondColumn[i * 2 + 3], Qt::AlignLeft | Qt::TextWordWrap, human.relationsInfo.keys()[i]);

    roboFontBold->setPointSize(20);
    painter->setFont(*roboFont);


    for (int i = 0; i < descriptions.length(); i++)
        painter->drawText(*secondColumn[i * 2 + 4], Qt::AlignLeft | Qt::TextWordWrap, descriptions[i]);

    // ADD INDEX
    roboFont->setPointSize(5);
    painter->setFont(*roboFont);

    // Debug
    QString indexStr = QString::number(human.sexIndex) + " ";
    if (human.relationshipIndex != -1)
        indexStr += QString::number(human.relationshipIndex) + " ";
    else indexStr += "__ ";
    if (human.organizationIndex != -1)
        indexStr += QString::number(human.organizationIndex);
    else indexStr += "__";
    painter->drawText(*index, indexStr);
}

//Print
void Printer::print(QVector<Human> humanList){
    painter = new QPainter(printer);
    foreach (Human human, humanList) {
        addPage(human);
    }
    painter->end();
    delete painter;
}

void Printer::drawRectSeparators(int x, int size){
    int y = 0;
    painter->setBrush(QBrush(Qt::black, Qt::SolidPattern));
    while (y < printer->height())
    {
        painter->drawRect(x, y, size, size);
        y += 2 * size;
    }
}

// Setup column
void Printer::setupFirstColumn(){
    firstColumn[0] = new QRect(68, 81, 950, 221);           // RED      ||      Имя                                         ||  20, BOLD, CAPS
    firstColumn[1] = new QRect(68, 325, 950, 379);          // RED      ||      Статус в семье                              ||  13, BOLD
    firstColumn[2] = new QRect(68, 445, 950, 512);          // RED      ||      Должность                                   ||  13
    firstColumn[3] = new QRect(68, 562, 950, 865);          // RED      ||      Место жительства                            ||  13
    firstColumn[4] = new QRect(68, 734, 950, 681);          // BLACK    ||      "Семья:"                                    ||  18, BOLD, CAPS
    firstColumn[5] = new QRect(68, 900, 950, 1020);         // RED      ||      Имя 1 члена семьи                           ||  13
    firstColumn[6] = new QRect(68, 1050, 950, 1170);        // RED      ||      Имя 2                                       ||  13
    firstColumn[7] = new QRect(68, 1210, 950, 1330);        // RED      ||      Имя 3                                       ||  13
    firstColumn[8] = new QRect(68, 1402, 950, 1521);        // RED      ||      Имя 4                                       ||  13
    firstColumn[9] = new QRect(68, 1632, 950, 2407);        // RED      ||      Инфа о семье                                ||  13 по правому краю без переносов

    index = new QRect(68, 2410, 950, 2460);
}

void Printer::setupSecondColumn(){
    int firstPointX = 1157;
    int secondPointX = 1200;
    secondColumn[0] = new QRect(firstPointX, 81, secondPointX, 215);       // BLACK    ||      "О ком ты думаешь?"          ||  18, BOLD, CAPS
    secondColumn[1] = new QRect(firstPointX, 228, secondPointX, 407);      // RED      ||      Имя 1                        ||  13
    secondColumn[2] = new QRect(firstPointX, 422, secondPointX, 1271);     // RED      ||      Статус 1                     ||  13
    secondColumn[3] = new QRect(firstPointX, 1279, secondPointX, 1478);    // RED      ||      Имя 2                        ||  13
    secondColumn[4] = new QRect(firstPointX, 1481, secondPointX, 1842);    // RED      ||      Статус 2                     ||  13
    secondColumn[5] = new QRect(firstPointX, 1848, secondPointX, 2045);    // RED      ||      Имя 3                        ||  13
    secondColumn[6] = new QRect(firstPointX, 2049, secondPointX, 2410);    // RED      ||      Статус 3                     ||  13
}

void Printer::setupThirdColumn(){
    thirdColumn[0] = new QRect(2475, 77, 3140, 215);        // BLACK    ||      "Деятельность"                              ||  18, BOLD, CAPS
    thirdColumn[1] = new QRect(2475, 288, 950, 1665);       // RED      ||      Описание деятельности                       ||  13
    thirdColumn[2] = new QRect(2475, 1788, 3140, 1924);     // BLACK    ||      "Распорядок дня"                            ||  13, BOLD, CAPS
    thirdColumn[3] = new QRect(2475, 2005, 3140, 2117);     // BLACK    ||      "Утром ты завтракаешь дома"                 ||  13
    thirdColumn[4] = new QRect(2475, 2145, 3140, 2257);     // RED      ||      День                                        ||  13
    thirdColumn[5] = new QRect(2475, 2295, 3140, 2407);     // RED      ||      Вечер                                       ||  13
}
