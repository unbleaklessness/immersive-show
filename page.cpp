#include "page.h"

Page::Page()
{
    setFixedSize(1170, 827);
    brush.setTextureImage(QImage(":/Images/Images/backGround.png"));
    palette.setBrush(QPalette::Window, brush);
    this->setPalette(palette);

    setAutoFillBackground(true);
    setFirstColumn();
    setSecondColumn();
    setThirdColumn();

    mainLayout = new QHBoxLayout;

    mainLayout->addLayout(firstColumn);
    mainLayout->addLayout(secondColumn);
    mainLayout->addLayout(thirdColumn);

    setLayout(mainLayout);
}

Page::~Page(){
    delete [] names;
    delete [] thinkName;
    delete [] textAbout;
}

void Page::setFirstColumn()
{
    firstColumn = new QVBoxLayout;
    firstColumn->setAlignment(Qt::AlignTop);

    name = new QTextEdit;
    name->setFixedSize(297, 46);
    firstColumn->addWidget(name);

    familyStatus = new QTextEdit;
    familyStatus->setFixedSize(297, 51);
    firstColumn->addWidget(familyStatus);

    work = new QTextEdit;
    work->setFixedSize(297, 30);
    firstColumn->addWidget(work);

    location = new QTextEdit;
    location->setFixedSize(297, 40);
    firstColumn->addWidget(location);

    QLabel *label = new QLabel("Семья:");
    label->setFixedSize(297, 44);
    firstColumn->addWidget(label);

    names = new QTextEdit [4];

    for(int i = 0; i < 4; i++){
        names[i].setFixedSize(297, 40);
        firstColumn->addWidget(&names[i]);
    }

    relationShip = new QTextEdit;
    relationShip->setFixedSize(297, 259);
    firstColumn->addWidget(relationShip);

}

void Page::setSecondColumn()
{
    secondColumn = new QVBoxLayout;
    secondColumn->setAlignment(Qt::AlignTop);

    QLabel *label = new QLabel("О ком ты думаешь?");
    label->setFixedSize(294, 44);
    secondColumn->addWidget(label);

    thinkName = new QTextEdit[3];

    textAbout = new QTextEdit[3];
    textAbout[0].setFixedSize(294, 283);
    textAbout[1].setFixedSize(294, 120);
    textAbout[2].setFixedSize(294, 120);
    for(int i = 0; i < 3; i++){
        thinkName[i].setFixedSize(294, 50);
        secondColumn->addWidget(&thinkName[i]);
        secondColumn->addWidget(&textAbout[i]);
    }
}

void Page::setThirdColumn(){
    thirdColumn = new QVBoxLayout;
    thirdColumn->setAlignment(Qt::AlignTop);

    QLabel *label = new QLabel("Деятельность");
    label->setFixedSize(295, 46);
    thirdColumn->addWidget(label);

    organizationDescription = new QTextEdit;
    organizationDescription->setFixedSize(295, 459);
    thirdColumn->addWidget(organizationDescription);

    QLabel *dayPlan = new QLabel("Распорядок дня:");
    dayPlan->setFixedSize(295, 45);
    thirdColumn->addWidget(dayPlan);

    morning = new QTextEdit;
    morning->setFixedSize(295, 37);
    thirdColumn->addWidget(morning);

    day = new QTextEdit;
    day->setFixedSize(295, 37);
    thirdColumn->addWidget(day);

    QLabel *evening = new QLabel("Вечером ты с теми, о ком ты думаешь");
    evening->setFixedSize(295, 37);
    thirdColumn->addWidget(evening);
}

void Page::setHuman(Human human){
    this->human = human;

    // First Column
    name->setText(human.name);
    familyStatus->setText(human.familyStatus);
    work->setText(human.work);
    location->setText(human.location);

    int j = 0;
    for(int i = 0; i < human.familyMembersNames.size(); i++){
        if(human.name != human.familyMembersNames[i]){
            names[j].setText(human.familyMembersNames[i]);
            j++;
        }
    }
    relationShip->setText(human.relationship);
    // Second Column
    QList<QString> namesList = human.relationsInfo.keys();

    for(int i = 0; i < namesList.size(); i++){
        thinkName[i].setText(namesList[i]);
        textAbout[i].setText(human.relationsInfo.take(namesList[i]));

    }
    // Third Column
    organizationDescription->setText(human.organizationDescription);
    morning->setText(human.morning);
    day->setText(human.day);

}

Human Page::getHuman(){
    // First Column
    human.name = name->toPlainText();
    human.familyStatus = familyStatus->toPlainText();
    human.work = work->toPlainText();
    human.location = location->toPlainText();
    human.relationship = relationShip->toPlainText();
    human.familyMembersNames.clear();
    for(int i = 0; i < 4; i++){
        if(names[i].toPlainText() != ""){
            human.familyMembersNames.push_back(names[i].toPlainText());
        }
    }
    // Second Column
    human.relationsInfo.clear();
    for(int i = 0; i < 3; i++){
        if(thinkName[i].toPlainText() != ""){
            human.relationsInfo.insert(thinkName[i].toPlainText(), textAbout[i].toPlainText());
        }
    }
    // Third Column
    human.organizationDescription = organizationDescription->toPlainText();
    human.morning = morning->toPlainText();
    human.day = day->toPlainText();

    return this->human;
}

void Page::clean(){
    delete [] names;
    delete [] thinkName;
    delete [] textAbout;

}













