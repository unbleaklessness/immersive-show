#ifndef GENERATEDIALOG_H
#define GENERATEDIALOG_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpinBox>
#include <QLabel>
#include <QPushButton>

class GenerateDialog : public QWidget
{
    Q_OBJECT
private:
    QPushButton *cancel;
    QPushButton *generate;
    QSpinBox *males;
    QSpinBox *females;
    QLabel *malesLBL;
    QLabel *femalesLBL;

    QVBoxLayout *rootLayout;
    QHBoxLayout *malesLayout;
    QHBoxLayout *femalesLayout;
    QHBoxLayout *buttonsLayout;

public:
    GenerateDialog(QWidget *parent = 0);

signals:
    void generateRoles(int males, int females);
private slots:
    void sendRoles();
    void setupConnections();
};

#endif // GENERATEDIALOG_H
