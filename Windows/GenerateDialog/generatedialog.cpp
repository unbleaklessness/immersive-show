#include "generatedialog.h"

GenerateDialog::GenerateDialog(QWidget *parent) : QWidget(parent)
{
    setFixedSize(275, 160);
    // Setup
    rootLayout = new QVBoxLayout;
    rootLayout->setAlignment(Qt::AlignTop);

    malesLayout = new QHBoxLayout;
    malesLayout->setAlignment(Qt::AlignLeft);
    males = new QSpinBox;
    males->setMinimum(4);
    males->setMaximum(26);
    malesLBL = new QLabel("Количество мужчин");
    malesLayout->addWidget(males);
    malesLayout->addWidget(malesLBL);

    femalesLayout = new QHBoxLayout;
    femalesLayout->setAlignment(Qt::AlignLeft);
    females = new QSpinBox;
    females->setMinimum(4);
    females->setMaximum(26);
    femalesLBL = new QLabel("Количество женщин");
    femalesLayout->addWidget(females);
    femalesLayout->addWidget(femalesLBL);

    buttonsLayout = new QHBoxLayout;
    buttonsLayout->setAlignment(Qt::AlignBottom);
    generate = new QPushButton("Сгенерировать");
    cancel = new QPushButton("Отмена");
    buttonsLayout->addWidget(generate);
    buttonsLayout->addWidget(cancel);

    rootLayout->addLayout(malesLayout);
    rootLayout->addLayout(femalesLayout);
    rootLayout->addLayout(buttonsLayout);

    setLayout(rootLayout);

    setupConnections();

}

void GenerateDialog::sendRoles(){
    int malesNumber = males->value();
    int femalesNumber = females->value();

    if(((malesNumber + femalesNumber) % 2 != 0) || (malesNumber + femalesNumber > 30)) {
        return;
    }

    emit generateRoles(malesNumber, femalesNumber);

}

void GenerateDialog::setupConnections(){
    connect(generate, SIGNAL(clicked(bool)), this, SLOT(sendRoles()));
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(close()));
}
