#ifndef PAGE_H
#define PAGE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QTextEdit>
#include <QLabel>
#include <QDebug>

#include "human.h"

class Page : public QWidget
{
    Q_OBJECT
private:
    Human human;

    // Text Edits
    //=========== First Column
    QTextEdit *name;
    QTextEdit *familyStatus;
    QTextEdit *work;
    QTextEdit *location;
    QTextEdit *names;
    QTextEdit *relationShip;
    //=========== Second Column
    QTextEdit *thinkName;
    QTextEdit *textAbout;
    //=========== Third Column
    QTextEdit *organizationDescription;
    QTextEdit *morning;
    QTextEdit *day;

    // Layouts
    QHBoxLayout *mainLayout;

    QVBoxLayout *firstColumn;
    QVBoxLayout *secondColumn;
    QVBoxLayout *thirdColumn;

    // Methods
    void setFirstColumn();
    void setSecondColumn();
    void setThirdColumn();


    // OTHER
    QBrush brush;
    QPalette palette;

public:
    Page();
    ~Page();

    void setHuman(Human human);
    void clean();
    Human getHuman();
};

#endif // PAGE_H
