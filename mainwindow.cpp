#include "mainwindow.h"

#include "page.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setMinimumSize(1280, 720);
    setupMainWindow();

    printer = new Printer;
    connect(topPanel->printButton->button, SIGNAL(clicked(bool)), this, SLOT(generateScriptPdf()));

    setupRoledScrollPanel();


}

MainWindow::~MainWindow()
{


}

void MainWindow::addPage(Human human)
{
    qDebug() << "In addPage";
    Page *tmp_page = new Page;
    tmp_page->setHuman(human);
    pagesVector.append(tmp_page);
}

void MainWindow::setupRoledScrollPanel()
{
    scrollLayout = new QVBoxLayout;
    scrollWGT = new QWidget;

    scrollLayout->setAlignment(Qt::AlignTop);
    scrollWGT->setLayout(scrollLayout);

}

void MainWindow::clearScrollPanel()
{
    delete scrollWGT;
    setupRoledScrollPanel();
    pagesVector.clear();
}

void MainWindow::generateScriptPdf()
{
    if(pagesVector.size() != 0){
        QVector<Human> humanVector;
        for(int i = 0; i < pagesVector.size(); i++){
            humanVector.append(pagesVector[i]->getHuman());
        }
        printer->print(humanVector);
    }
}

void MainWindow::showRole(Human human)
{
    qDebug() << human.name;
    qDebug() << human.familyStatus;
    qDebug() << human.work;
    qDebug() << human.location;
    for(int i = 0; i < human.familyMembersNames.size(); i++){
        qDebug() << human.familyMembersNames[i];
    }
    qDebug() << human.relationship;
    QList<QString> namesList = human.relationsInfo.keys();
    for(int i = 0; i < namesList.size(); i++){
        qDebug() << namesList[i];
        qDebug() << human.relationsInfo.take(namesList[i]);
    }
    qDebug() << human.organizationDescription;
    qDebug() << human.morning;
    qDebug() << human.day;
}

void MainWindow::showRoles()
{
    foreach (Page *page, pagesVector) {
        scrollLayout->addWidget(page);
    }
    pagesScrollArea->setWidget(scrollWGT);
}

void MainWindow::createRoles(int males, int females)
{
    clearScrollPanel();

    Families families = familyFactory->getFamilies();
    Relationships relationships = relationshipFactory->getRelationships();

    FamiliesHandler::configure(families, males, females);
    RelationshipsDistributor::distribute(relationships, families);

    convertor.setFamilies(families);

    QVector<Human> humansVector = convertor.getConvertedMembers();

    foreach (Human human, humansVector) {
        addPage(human);
    }

    FamiliesHandler::debug(families);
    showRoles();

}

void MainWindow::setupMainWindow(){
    // Setup central panel
    centralWgt = new QWidget;
    centralLayout = new QVBoxLayout;
    centralLayout->setAlignment(Qt::AlignTop);
    centralLayout->setMargin(0);
    centralWgt->setLayout(centralLayout);

    // Setup pages scroll layout
    pagesScrollArea = new QScrollArea;
    pagesScrollArea->setAlignment(Qt::AlignCenter);
    pagesScrollArea->setMinimumSize(1170, 827);

    setupRoledScrollPanel();
    pagesScrollArea->setWidget(scrollWGT);

    topPanel = new TopPanel;
    centralLayout->addWidget(topPanel);
    centralLayout->addWidget(pagesScrollArea);

    this->setCentralWidget(centralWgt);
}
