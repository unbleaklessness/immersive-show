#ifndef PRINTER_H
#define PRINTER_H

#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QFont>
#include <QFontDatabase>
#include <QObject>
#include <QWidget>
#include <QDebug>

#include "human.h"


class Printer : public QWidget
{
    Q_OBJECT

public:
     Printer();


     void print(QVector<Human> humanList);


private:

     QFont *roboFont;
     QFont *roboFontBold;

     // Print
     QPrinter *printer;
     QPrintDialog *printDialog;
     QPainter *painter;

     QRect *firstColumn[10];
     QRect *secondColumn[7];
     QRect *thirdColumn[6];
     QRect *index;

     void setupPrinter();

     void drawRectSeparators(int x, int size);
     void addPage(Human Human);

     // Setup Column
     void setupFirstColumn();
     void setupSecondColumn();
     void setupThirdColumn();

signals:

     void finished();
};

#endif // PRINTER_H
