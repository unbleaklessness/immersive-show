#ifndef CONVERTFAMILIES_H
#define CONVERTFAMILIES_H

#include <QVector>
#include "human.h"

#include "Generator/family.h"


class ConvertFamilies
{
private:
    QVector<Human> humanVector;
    Human human;

public:
    ConvertFamilies();

    void setFamilies(Families familys);
    QVector<Human> getConvertedMembers();

private:
    void parseFamily(Family *family);
    void parseMember(Member *member);


};

#endif // CONVERTFAMILIES_H
