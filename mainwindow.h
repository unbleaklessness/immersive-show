#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScrollArea>

#include "page.h"
#include "printer.h"
#include "convertfamilies.h"
#include "Elements/TopPanel/toppanel.h"

#include "Generator/familieshandler.h"
#include "Generator/relationshipfactory.h"
#include "Generator/familyfactory.h"
#include "Generator/relationshipsdistributor.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    TopPanel *topPanel;
private:
    QWidget *centralWgt;
    QVBoxLayout *centralLayout;

    QScrollArea *pagesScrollArea;
    QVBoxLayout *scrollLayout;
    QWidget *scrollWGT;

    QVector<Page*> pagesVector;
    Printer* printer;
    ConvertFamilies convertor;

    FamilyFactory* familyFactory;
    RelationshipFactory* relationshipFactory;



    void setupMainWindow();

    void showRole(Human human);
    void setupRoledScrollPanel();
    void clearScrollPanel();


private slots:

    void generateScriptPdf();

public slots:
    void addPage(Human human);
    void showRoles();

    void createRoles(int males, int females);

signals:


};

#endif // MAINWINDOW_H
