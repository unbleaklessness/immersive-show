#include "mainwindow.h"
#include <QApplication>

#include "Elements/Button/buttontoppanel.h"


#include "Window/GenerateDialog/generatedialog.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


    MainWindow mainwindow;
    GenerateDialog generateDialog;

    QObject::connect(mainwindow.topPanel->generateRoles->button, SIGNAL(clicked(bool)),
                     &generateDialog, SLOT(show()));
    QObject::connect(&generateDialog, SIGNAL(generateRoles(int,int)), &mainwindow, SLOT(createRoles(int,int)));

    mainwindow.show();

    return a.exec();
}
