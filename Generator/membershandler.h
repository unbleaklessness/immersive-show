#ifndef MEMBERSHANDLER_H
#define MEMBERSHANDLER_H

#include <QObject>

#include "member.h"
#include "helpers.h"

class Member;
typedef QVector<Member*> Members;

class MembersHandler : public QObject
{
    Q_OBJECT

public:
    static Members filterByTags(Members members, Tags tags);
};

#endif // MEMBERSHANDLER_H
