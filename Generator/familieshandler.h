#ifndef FAMILIES_HANDLER_H
#define FAMILIES_HANDLER_H

#include <QtDebug>
#include <QObject>
#include <QVector>

#include <algorithm>

#include "family.h"
#include "helpers.h"

class FamiliesHandler : public QObject
{
    Q_OBJECT

private:
    static QVector<QVector<QString>> getOrganizationsData();
    static void addFirstAndSecondPersonTags(Families families);
    static void addBeautyAndBeastTags(Families families);
    static void addNames(Families families);
    static void addMarriedTags(Families families);
    static void addOrganizations(Families families);
    static void addFamilyStatuses(Families families);
    static void addFamilyDescriptions(Families families);
    static void addMorningRoutines(Families families);
    static void addEveningRoutines(Families families);
    static void addStudents(Families families);
    static void addLivingPlaces(Families families);
    static void addVolunteerTags(Families families);

public:
    static void configure(Families families, int males, int females);
    static void populate(Families families, int males, int females);

    static QVector<Family*> getFamilyByName(Families families, QString name);
    static QVector<Family*> getFamilyByFamilyTag(Families families, FamilyTag familyTag);
    static int iWithFewer(Families families, Tags tags = Tags({}));
    static Members getWorklessMembers(Families families);

    static void debug(Families families);
};

#endif // FAMILIES_HANDLER_H
