#include "familieshandler.h"

void FamiliesHandler::populate(Families families, int males, int females)
{
    int peoples = males + females;
    if (peoples <= 15) families.removeLast();
    if (peoples <= 25) families.removeLast();

    int m = std::min(males, females);
    int g = std::max(males, females);

    Tag gGender;
    Tag mGender;

    if (males > females)
        gGender = Tag::Male,
        mGender = Tag::Female;
    else
        gGender = Tag::Female,
        mGender = Tag::Male;

    families[0]->addMember(Tags({Tag::Male, Tag::Adult}));
    families[0]->addMember(Tags({Tag::Female, Tag::Adult}));
    families[0]->addMember(Tags({Tag::Male, Tag::Child}));
    families[0]->addMember(Tags({Tag::Female, Tag::Child}));
    families[1]->addMember(Tags({Tag::Male, Tag::Adult}));
    families[1]->addMember(Tags({Tag::Female, Tag::Adult}));
    families[1]->addMember(Tags({Tag::Male, Tag::Child}));
    families[1]->addMember(Tags({Tag::Female, Tag::Child}));
    g -= 4; m -= 4;

    int c = 2;
    while (m != 0 && c != 4 && g != 0)
    {
        int random = Helpers::randInt(0, 1);
        if (random == 0)
        {
            families[c]->addMember(Tags({Tag::Male, Tag::Child}));
            families[c]->addMember(Tags({Tag::Female, Tag::Adult}));
        }
        else
        {
            families[c]->addMember(Tags({Tag::Male, Tag::Adult}));
            families[c]->addMember(Tags({Tag::Female, Tag::Child}));
        }
        c += 1; m -= 1; g -= 1;
    }

    while (c != 4 && g > 0)
    {
        families[c]->addMember(Tags({gGender, Tag::Child}));
        families[c]->addMember(Tags({gGender, Tag::Adult}));
        c += 1; g -= 2;
    }

    while (m != 0 && c != families.length() && g != 0)
    {
        int random = Helpers::randInt(0, 1);
        if (random == 0)
        {
            families[c]->addMember(Tags({Tag::Male, Tag::Child}));
            families[c]->addMember(Tags({Tag::Female, Tag::Adult}));
        }
        else
        {
            families[c]->addMember(Tags({Tag::Male, Tag::Adult}));
            families[c]->addMember(Tags({Tag::Female, Tag::Child}));
        }
        c += 1; m -= 1; g -= 1;
    }

    while (c != families.length() && g > 0)
    {
        families[c]->addMember(Tags({gGender, Tag::Child}));
        families[c]->addMember(Tags({gGender, Tag::Adult}));
        c += 1; g -= 2;
    }

    for (int i = 0; g != 0; i++, g--)
    {
        int minM = iWithFewer(families);
        int minC = iWithFewer(families, Tags({Tag::Child}));
        if (families[minM]->nGet(Tags({gGender, Tag::Adult})) == 0)
            families[minM]->addMember(Tags({gGender, Tag::Adult}));
        else
            if (families[minC]->nGet(Tags({Tag::Child})) < 3)
                families[minC]->addMember(Tags({gGender, Tag::Child}));
    }

    for (int i = 0; m != 0; i++, m--)
    {
        int minM = iWithFewer(families);
        int minC = iWithFewer(families, Tags({Tag::Child}));
        if (families[minM]->nGet(Tags({mGender, Tag::Adult})) == 0)
            families[minM]->addMember(Tags({mGender, Tag::Adult}));
        else
            if (families[minC]->nGet(Tags({Tag::Child})) < 3)
                families[minC]->addMember(Tags({mGender, Tag::Child}));
    }
}

void FamiliesHandler::addLivingPlaces(Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        if (families[i]->getFamilyTag() == FamilyTag::Red)
            for (int j = 0; j < members.length(); j++)
                members[j]->setLivingPlace("Ты живешь в доме Рэд");
        if (families[i]->getFamilyTag() == FamilyTag::Wood)
            for (int j = 0; j < members.length(); j++)
                members[j]->setLivingPlace("Ты живешь в доме Вуд");
        if (families[i]->getFamilyTag() == FamilyTag::Ostin)
            for (int j = 0; j < members.length(); j++)
                members[j]->setLivingPlace("Ты живешь в доме Остин");
        if (families[i]->getFamilyTag() == FamilyTag::Black)
            for (int j = 0; j < members.length(); j++)
                members[j]->setLivingPlace("Ты живешь в доме Блэк");
        if (families[i]->getFamilyTag() == FamilyTag::Smith)
            for (int j = 0; j < members.length(); j++)
                members[j]->setLivingPlace("Ты живешь в доме Смит");
        if (families[i]->getFamilyTag() == FamilyTag::Li)
            for (int j = 0; j < members.length(); j++)
                members[j]->setLivingPlace("Ты живешь в доме Ли");
    }
}

void FamiliesHandler::addMarriedTags(Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        if (families[i]->firstPersonExists() &&
                families[i]->secondPersonExists())
        {
            families[i]->getFirstPerson()->addTag(Tag::Married);
            families[i]->getSecondPerson()->addTag(Tag::Married);
        }
    }
}

void FamiliesHandler::addBeautyAndBeastTags(Families families)
{
    Members members;
    for (int i = 0; i < families.length(); i++)
    {
        QString familyName = families[i]->getFamilyName();
        if (familyName == "Рэд")
        {
            members = families[i]->get(Tags({Tag::Male, Tag::Adult}));
            if (members.length() != 0)
                members[0]->addTag(Tag::Beast);
            members = families[i]->get(Tags({Tag::Child}));
            for (int j = 0; j < members.length() && j < 2; j++)
                members[j]->addTag(Tag::Beast);
        }
        else if (familyName == "Вуд")
        {
            members = families[i]->get(Tags({Tag::Child}));
            for (int j = 0; j < members.length(); j++)
                members[j]->addTag(Tag::Beauty);
        }
        else if (familyName == "Остин")
        {
            members = families[i]->get(Tags({Tag::Child}));
            for (int j = 0; j < members.length() && j < 2; j++)
                members[j]->addTag(Tag::Beast);
        }
        else if (familyName == "Блэк")
        {
            members = families[i]->get(Tags({Tag::Male, Tag::Adult}));
            if (members.length() != 0)
                members[0]->addTag(Tag::Beast);
            members = families[i]->get(Tags({Tag::Child}));
            if (members.length() == 2)
                members[1]->addTag(Tag::Beauty);
        }
        else if (familyName == "Ли")
        {
            members = families[i]->get(Tags({Tag::Child}));
            if (members.length() != 0)
                members[0]->addTag(Tag::Beast);
        }
    }
}

void FamiliesHandler::configure(Families families, int males, int females)
{
    populate(families, males, females);
    addFirstAndSecondPersonTags(families);
    addBeautyAndBeastTags(families);
    addNames(families);
    addMarriedTags(families);
    addFamilyStatuses(families);
    addLivingPlaces(families);
    addFamilyDescriptions(families);
    addMorningRoutines(families);
    addEveningRoutines(families);
    addStudents(families);
    addOrganizations(families);
    addVolunteerTags(families);
}

void FamiliesHandler::addVolunteerTags(Families families)
{
    bool isAdded = false;
    for (int i = 0; i < families.length(); i++)
    {
        Members maleStudents = families[i]->get(Tags({Tag::Student, Tag::Male}));
        if (maleStudents.length() != 0 && isAdded == false)
        {
            maleStudents[0]->addTag(Tag::Volunteer);
            isAdded = true;
            continue;
        }
        Members femaleStudents = families[i]->get(Tags({Tag::Student, Tag::Female}));
        if (femaleStudents.length() != 0 && isAdded == true)
        {
            femaleStudents[0]->addTag(Tag::Volunteer);
            break;
        }
    }
}

void FamiliesHandler::addMorningRoutines(Families families)
{
    QString initial = "Завтрак с семьей ";
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
            members[j]->setMorningRoutine(initial + families[i]->getFamilyName());
    }
}

void FamiliesHandler::addEveningRoutines(Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
            members[j]->setEveningRoutine("Вечером ты с теми,\nо ком ты думаешь");
    }
}

QVector<QVector<QString>> FamiliesHandler::getOrganizationsData()
{
    typedef QVector<QString> Org;
    typedef QVector<Org> Orgs;
    Orgs organizationsData;
    
    organizationsData.append(Org({
        "Мэрия",
        "Шериф",
        "Работаешь в мэрии",
        "В этом городе все далеко не так благополучно, как может показаться. Кто-то из студентов точно приторговывает наркотой, несовершеннолетние тусуются в барах с алкоголем и ночуют на набережной. А впереди выборы, которые должны пройти без обмана, и фестиваль, от которого неизвестно чего ждать.  Тебе уже давно нужен помощник. Может быть, взять кто-то из студентов? Работа достойная, и таким же должен быть кандидат."
    }));
    organizationsData.append(Org({
        "Мэрия",
        "Пресс-секретарь",
        "Работаешь в мэрии",
        "Ты - связующее звено между мэрией и городом. Что бы ни решили на планерке - последнее слово всегда останется за тобой, потому что именно ты передаешь сводку новостей в агентство в конце рабочего дня. Ты отвечаешь за предвыборную компанию мэра, но он явно не справляется со своими обязанностями. Главой города должен стать более подходящий кандидат. Ты найдешь такого среди горожан и поможешь ему выиграть голосование."
    }));
    organizationsData.append(Org({
        "Мэрия",
        "Курьер при мэрии",
        "Работаешь в мэрии",
        "У тебя есть работа, пусть не самая престижная, но это лучше, чем просиживать штаны на лекциях. Теперь главное - не терять времени даром и занять более высокий пост. Для этого нужно проявлять активность, вникать в проблемы и помогать их решать. А еще было бы не плохо не вылететь из универа..."
    }));

    organizationsData.append(Org({
        "Новостное агенство",
        "Корреспондент новостей",
        "В новостном агенстве",
        "Ты - глаза и уши этого города, ты успеваешь быть всегда и везде. Кому интересно слушать о том, что в семье мэра на завтрак ели яичницу? Жареные факты - вот, чего от тебя все ждут. Ты не стесняешься самостоятельно раздобыть новости и приукрасить их, добавив огонька. И часто ты провоцируешь горожан на яркие поступки, о которых можно будет рассказать. Скоро твой шеф станет мэром и отдаст пост директора тому, кого сочтет наиболее талантливым. Конечно, это будешь ты!"
    }));
    organizationsData.append(Org({
        "Новостное агенство",
        "Специалист по рекламе",
        "В новостном агенстве",
        "В городе выборы. Ваш шеф метит в мэры, и все твои коллеги-болваны его поддерживают, мечтая занять директорское место. Тебе же совершенно не нужны все эти дрязги. Твое дело - рекламный отдел и твои проценты от продажи. Так что ты не видишь повода отказывать другим кандидатам в предвыборной рекламе в новостях. Более того - ты активно ищешь клиентов, которым нужны услуги твоего отдела."
    }));
    organizationsData.append(Org({
        "Новостное агенство",
        "Стажер",
        "В новостном агенстве",
        "А теперь ты - глаза и уши этого города, ты успеваешь быть всегда и везде. Кому интересно слушать о том, что в семье мэра на завтрак ели яичницу? Жареные факты - вот, чего от тебя все ждут. Ты не стесняешься самостоятельно раздобыть новости и приукрасить их, добавив огонька. И часто ты провоцируешь горожан на яркие поступки, о которых можно будет рассказать. Скоро твой шеф станет мэром и отдаст пост директора тому, кого сочтет наиболее талантливым. Конечно, это будешь ты!"
    }));

    organizationsData.append(Org({
        "Мотель",
        "Официант",
        "Работаешь в отеле",
        "Хозяин хочет, чтобы тут все было идеально. Ты ответственный работник, любишь порядок... но всему есть предел! Нельзя круглые сутки торчать на работе! Жизнь пройдет, а ты за стойкой.... Ты пытаешься скинуть с себя хотя бы часть дел. Пусть другие тоже вкалывают! А ты придумаешь себе занятие поинтереснее."
    }));
    organizationsData.append(Org({
        "Мотель",
        "Помощник бармена",
        "Работаешь в отеле",
        "У тебя есть работа, пусть не самая престижная, но это лучше, чем сидеть за партой. Ты не против нести ответственность и вкалывать... Но кроме работы есть еще учеба и личная жизнь... Нельзя же отказываться от всего этого!  То, что ты - всего лишь стажер -  не повод свалить на тебя всю самую грязную работу. В жизни есть более важные вещи."
    }));
    organizationsData.append(Org({
        "Мотель",
        "Помощник официанта",
        "Работаешь в отеле",
        "У тебя есть работа, пусть не самая престижная, но это лучше, чем просиживать штаны на лекциях. Ты не против нести ответственность и вкалывать... Но кроме работы есть еще учеба и личная жизнь... Нельзя же отказываться от всего этого!  То, что ты - всего лишь стажер -  не повод свалить на тебя всю самую грязную работу. В жизни есть более важные вещи."
    }));

    organizationsData.append(Org({
        "Клиника",
        "Заместитель главного врача",
        "Работаешь в клинике",
        "Ты искренне заинтересован в развитии клиники, и пока владелец спасает молодежь от наркотиков, ты печешься о наличии клиентов, рекламе, правильном пиаре, а так же налаживаешь общение с властями. Ведь если поддержать правильного человека на выборах, это может открыть новые перспективы для клиники... и твоей зарплаты."
    }));
    
    return organizationsData;
}

void FamiliesHandler::addOrganizations(Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        FamilyTag familyTag = families[i]->getFamilyTag();
        if (!families[i]->firstPersonExists()) continue;
        Member* firstPerson = families[i]->getFirstPerson();
        if (familyTag == FamilyTag::Red)
        {
            firstPerson->setOrganization("Мэрия");
            firstPerson->setOrganizationPosition("Мэр города");
            firstPerson->setDayRoutine("Работаешь в мэрии");
            firstPerson->setOrganizationDescription("Каждый рабочий день ты начинаешь с планерки, на которой принимаются решения о жизни города и работе мэрии. Впереди последняя неделя предвыборной гонки. Соперники явно что-то замышляют, но ты не отступишь. Ты не отдашь этот город кому попало! У тебя пропал аппетит, и ночами снятся кошмары, как кто-то из проходимцев завладел постом мэра и разрушает все, что ты сделал для города. И кем окажешься ты, если мэром станет кто-то другой?");
            firstPerson->setOrganizationIndex(1);
        }
        if (familyTag == FamilyTag::Wood)
        {
            firstPerson->setOrganization("Новостное агенство");
            firstPerson->setOrganizationPosition("Глава новостного агенства");
            firstPerson->setDayRoutine("В новостном агентстве");
            firstPerson->setOrganizationDescription("Каждое утро после общего выпуска новостей звучит местный выпуск. И что бы ни происходило в городе, он должен выйти в эфир! На этой неделе выборы мэра. Но ты-то знаешь, кто УЖЕ фактически управляет умами горожан. Ты - лучший кандидат на этот пост, и пусть об этом узнают все! Самые горячие и шокирующие новости могут стать частью твоей предвыборной кампании. Предстоит хорошо потрудиться, но результат будет того стоить!");
            firstPerson->setOrganizationIndex(2);
        }
        if (familyTag == FamilyTag::Ostin)
        {
            firstPerson->setOrganization("Мотель");
            firstPerson->setOrganizationPosition("Хозяин мотеля и кафе");
            firstPerson->setDayRoutine("Работаешь в отеле");
            firstPerson->setOrganizationDescription("Город еще не знает, но ты вот-вот лишишься всего: мотель проигран в казино, новый хозяин объявится со дня на день. Последняя надежда на то, что тебе дадут должность управляющего мотеля, и тогда почти ничего не изменится, и может быть даже никто ничего не узнает... А для этого все должно быть на высшем уровне: обслуживание, еда, чистота, охрана и вечеринки... Вечеринок можно и побольше! И рекламы в новостях. Про твое заведение должен узнать каждый!");
            firstPerson->setOrganizationIndex(3);
        }
        if (familyTag == FamilyTag::Black)
        {
            firstPerson->setOrganization("Университет");
            firstPerson->setOrganizationPosition("Преподаватель в университете");
            firstPerson->setDayRoutine("Работаешь в университете");
            firstPerson->setOrganizationDescription("Каждый день начинается с переклички, потом - лекция. Ты стараешься задавать студентам побольше вопросов и приводить примеры из собственной жизни. В последнее время многие старшекурсники стали устраиваться на работу и пропускать лекции. Это тебя очень беспокоит! Только образование  поможет им устроиться в жизни! Обо всех, кто прогуливает занятия, ты рассказываешь ректору и родителям. Ты против всего, что отвлекает от учебы. Особенно - против этого дурацкого фестиваля. Ты будешь мешать ему любой ценой.");
            firstPerson->setOrganizationIndex(6);
        }
        if (familyTag == FamilyTag::Smith)
        {
            firstPerson->setOrganization("Клиника");
            firstPerson->setOrganizationPosition("Врач и глава частной клиники");
            firstPerson->setDayRoutine("Работаешь в клинике");
            firstPerson->setOrganizationDescription("Теперь у тебя собственная клиника! Ты строго следишь, чтобы все было идеально. Одна из проблем этого города - молодежь и наркотики. Ты уже предложил свою помощь, но кажется, что мэрии и Университету плевать - все слишком увлечены  будущими выборами. Тогда ты сам готов организовать работу психолога с молодежью и тотальную проверку. На днях вы возьмете кровь на анализы и оповестите родителей после результата экспертизы. ");
            firstPerson->setOrganizationIndex(4);
        }
    }
    for (int i = 0; i < families.length(); i++)
    {
        FamilyTag familyTag = families[i]->getFamilyTag();
        if (!families[i]->secondPersonExists()) continue;
        Member* secondPerson = families[i]->getSecondPerson();
        if (families.length() >= 4)
        {
            if (familyTag == FamilyTag::Ostin)
            {
                secondPerson->setOrganization("Мэрия");
                secondPerson->setOrganizationPosition("Глава департамента культуры");
                secondPerson->setDayRoutine("Работаешь в мэрии");
                secondPerson->setOrganizationDescription("Ты отвечаешь за культурную жизнь в этом городе и борешься с бескультурной. Молодежь - твои основные соратники и враги.  А проведение  молодежного фестиваля - твоя ежегодная радость и головная боль. Даже продюсера пригласили, чтобы все было идеально. Осталось проследить, чтобы он ничего не натворил. На этой неделе все буквально помешались на выборах! Но ты не позволишь этим дрязгам захватить весь город. Потому что есть вещи куда более важные, чем политика!");
            }
            if (familyTag == FamilyTag::Black)
            {
                secondPerson->setOrganization("Новостное агенство");
                secondPerson->setOrganizationPosition("Ведущий новостей");
                secondPerson->setDayRoutine("В новостном агенстве");
                secondPerson->setOrganizationDescription("Твой голос знает весь город. Каждое утро ты выступаешь в прямом эфире со свежими новостями. И поэтому последнее слово всегда остается за тобой) Ты умеешь вкусно подать любые факты. И это особенно полезно шефу, который претендует на пост мэра. Ты его поддерживаешь, потому что перед уходом он должен будет назначить нового директора, и им конечно же должен стать ты, а не какой-нибудь малолетний выскочка!");
            }
            if (familyTag == FamilyTag::Wood)
            {
                secondPerson->setOrganization("Университет");
                secondPerson->setOrganizationPosition("Ректор университета");
                secondPerson->setDayRoutine("Работаешь в университете");
                secondPerson->setOrganizationDescription("Ты кандидат на пост мэра и сделаешь все, чтобы победить. У тебя есть много рычагов влияния на настроение горожан. Каждый день ты проводишь перекличку среди студентов, чтобы напомнить о себе. По этой же причине ты помогаешь им в трудоустройстве и организации фестиваля. Впрочем, ты достаточно умен, чтобы любое событие в городе стало частью твоей предвыборной кампании: будь то тотальная проверка на наркотики, наводнение, землетрясение или эпидемия.");
            }
            if (familyTag == FamilyTag::Red)
            {
                Members children = families[i]->get(Tags({Tag::Child, Tag::Male}));
                if (children.length() != 0)
                {
                    children[0]->setOrganization("Мотель");
                    children[0]->setOrganizationPosition("Бармен");
                    children[0]->setDayRoutine("Работаешь в отеле");
                    children[0]->setOrganizationDescription("Хозяин хочет, чтобы тут все было идеально. Ты ответственный работник, любишь порядок... но всему есть предел! Нельзя круглые сутки торчать на работе! Жизнь идет, а ты за стойкой... Ты пытаешься скинуть с себя хотя бы часть дел. Пусть другие тоже вкалывают! А ты придумаешь себе занятие поинтереснее.");
                    children[0]->setOrganizationIndex(7);
                }
            }
        }
        if (families.length() >= 5)
        {
            if (familyTag == FamilyTag::Wood)
            {
                secondPerson->setOrganization("Мэрия");
                secondPerson->setOrganizationPosition("Глава департамента культуры");
                secondPerson->setDayRoutine("Работаешь в мэрии");
                secondPerson->setOrganizationDescription("Ты отвечаешь за культурную жизнь в этом городе и борешься с бескультурной. Молодежь - твои основные соратники и враги.  А проведение  молодежного фестиваля - твоя ежегодная радость и головная боль. Даже продюсера пригласили, чтобы все было идеально. Осталось проследить, чтобы он ничего не натворил. На этой неделе все буквально помешались на выборах! Но ты не позволишь этим дрязгам захватить весь город. Потому что есть вещи куда более важные, чем политика!");
            }
            if (familyTag == FamilyTag::Smith)
            {
                secondPerson->setOrganization("Университет");
                secondPerson->setOrganizationPosition("Ректор университета");
                secondPerson->setDayRoutine("Работаешь в университете");
                secondPerson->setOrganizationDescription("Ты кандидат на пост мэра и сделаешь все, чтобы победить. У тебя есть много рычагов влияния на настроение горожан. Каждый день ты проводишь перекличку среди студентов, чтобы напомнить о себе. По этой же причине ты помогаешь им в трудоустройстве и организации фестиваля. Впрочем, ты достаточно умен, чтобы любое событие в городе стало частью твоей предвыборной кампании: будь то тотальная проверка на наркотики, наводнение, землетрясение или эпидемия.");
            }
            if (familyTag == FamilyTag::Ostin)
            {
                secondPerson->setOrganization("Клиника");
                secondPerson->setOrganizationPosition("Психолог в частной клинике");
                secondPerson->setDayRoutine("Работаешь в клинике");
                secondPerson->setOrganizationDescription("Твое предназначение - работа с трудными подростками и их семьями. Очевидно, что многие семьи в городе находятся на грани краха. Дети Рэдов по слухам употребляют наркотики, Смиты-младшие в шоке от переезда, а Остины-старшие в последнее время явно что-то скрывают. К сожалению, люди не всегда понимают, что им необходима твоя помощь. Но ты умеешь найти правильный подход, помогаешь решить сложные вопросы и делаешь их счастливыми.");
            }
        }
    }

    typedef QVector<QString> Org;
    typedef QVector<Org> Orgs;
    Orgs orgsData = getOrganizationsData();
    Members workless = getWorklessMembers(families);

    for (int i = 0; i < workless.length(); i++)
    {
        if (orgsData.length() == 0) break;
        Org current = orgsData.takeLast();
        workless[i]->setOrganization(current[0]);
        workless[i]->setOrganizationPosition(current[1]);
        workless[i]->setDayRoutine(current[2]);
        workless[i]->setOrganizationDescription(current[3]);
    }
}

void FamiliesHandler::addStudents(Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        if (families[i]->getMembers().length() == 0) continue;
        Members children = families[i]->get({Tag::Child});

        if (children[0]->getOrganization() == "")
        {
            children[0]->setOrganization("Университет");
            children[0]->setOrganizationPosition("Студент");
            children[0]->setDayRoutine("Учишься в университете");
            children[0]->addTag(Tag::Student);

            if (children[0]->getGender() == Tag::Male)
                children[0]->setOrganizationDescription("Тебе надоело протирать штаны и слушать бестолковые лекции в универе. Хочется быть более самостоятельным и иметь в кармане хрустящие банкноты, чтобы пить коктейли в кафе и целоваться в кино... Или чтобы хоть как-то помогать родителям. Пришло время устроиться на работу!");
            else if (children[0]->getGender() == Tag::Female)
                children[0]->setOrganizationDescription("Тебе надоело протирать штаны и слушать бестолковые лекции в универе. Хочется быть более самостоятельным и иметь в кармане хрустящие банкноты, чтобы пить коктейли в кафе и целоваться в кино... Или чтобы хоть как-то помогать родителям. Пришло время устроиться на работу!");
        }

        for (int j = 1; j < children.length(); j++)
            if (children[j]->getOrganization() == "")
            {
                children[j]->setOrganization("Университет");
                children[j]->setOrganizationPosition("Студент");
                children[j]->setDayRoutine("Учишься в университете");
                children[j]->addTag(Tag::Student);

                if (children[j]->getGender() == Tag::Male)
                    children[j]->setOrganizationDescription("Тебе нравится учеба в универе. Ты развлекаешься с друзьями на скучных лекциях, тусуешься в отличной кампании и никому ничего не должен. Приближается фестиваль, и ты точно оттянешься на нем по полной. Можно собрать рок-группу, читать матерные стихи, танцевать... А заодно клеиться к девчонкам или с кем-нибудь подраться.");
                else if (children[j]->getGender() == Tag::Female)
                    children[j]->setOrganizationDescription("Тебе нравится учеба в универе. Ты развлекаешься с друзьями на скучных лекциях, тусуешься в отличной кампании и никому ничего не должна. Приближается фестиваль, и ты точно оттянешься на нем по полной. Можно собрать рок-группу, танцевать...  А заодно позажигать с парнями.");
            }
    }
}

Members FamiliesHandler::getWorklessMembers(Families families)
{
    Members result;
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
        {
            if (members[j]->getOrganization() == "" &&
                    members[j]->getMaturity() == Tag::Adult)
                result.append(members[j]);
        }

        if (families[i]->getFamilyTag() == FamilyTag::Wood ||
                families[i]->getFamilyTag() == FamilyTag::Ostin)
        {
            Members children = families[i]->get(Tags({Tag::Child}));
            if (children.length() != 0) result.append(children[0]);
        }

        if (families[i]->getFamilyTag() == FamilyTag::Li ||
                families[i]->getFamilyTag() == FamilyTag::Black)
        {
            Members children = families[i]->get(Tags({Tag::Child}));
            if (children.length() >= 2) result.append(children[1]);
        }
    }
    return result;
}

void FamiliesHandler::addFirstAndSecondPersonTags(Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        Members maleAdults = families[i]->get(Tags({Tag::Adult, Tag::Male}));
        Members femaleAdults = families[i]->get(Tags({Tag::Adult, Tag::Female}));

        if (maleAdults.length() != 0 &&
                femaleAdults.length() != 0)
        {
            maleAdults[0]->addTag(Tag::FirstPerson);
            femaleAdults[0]->addTag(Tag::SecondPerson);
            continue;
        }

        if (maleAdults.length() != 0)
            maleAdults[0]->addTag(Tag::FirstPerson);

        if (femaleAdults.length() != 0)
            femaleAdults[0]->addTag(Tag::FirstPerson);
    }
}

QVector<Family*> FamiliesHandler::getFamilyByName(Families families, QString name)
{
    QVector<Family*> result;
    for (int i = 0; families.length(); i++)
        if (families[i]->getFamilyName() == name) result.append(families[i]);
    return result;
}

QVector<Family*> FamiliesHandler::getFamilyByFamilyTag(Families families, FamilyTag familyTag)
{
    QVector<Family*> result;
    for (int i = 0; families.length(); i++)
        if (families[i]->getFamilyTag() == familyTag) result.append(families[i]);
    return result;
}

int FamiliesHandler::iWithFewer(Families families, Tags tags)
{
    int min = 0;
    if (tags.empty())
    {
        for (int i = 0; i < families.length(); i++)
           if (families[i]->nMembers() <= families[min]->nMembers()) min = i;
    }
    else
    {
        for (int i = 0; i < families.length(); i++)
           if (families[i]->nGet(tags) <= families[min]->nGet(tags)) min = i;
    }
    return min;
}

void FamiliesHandler::addNames(Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        FamilyTag familyTag = families[i]->getFamilyTag();
        Members maleAdults = families[i]->get(Tags({Tag::Male, Tag::Adult}));
        Members femaleAdults = families[i]->get(Tags({Tag::Female, Tag::Adult}));
        Members maleChildren = families[i]->get(Tags({Tag::Male, Tag::Child}));
        Members femaleChildren = families[i]->get(Tags({Tag::Female, Tag::Child}));
        if (familyTag == FamilyTag::Red)
        {
            if (maleAdults.length() >= 1) maleAdults[0]->setName("Мистер Эдвард Рэд");
            if (femaleAdults.length() >= 1) femaleAdults[0]->setName("Миссис Элисон Рэд");
            if (maleChildren.length() >= 1) maleChildren[0]->setName("Алан Рэд");
            if (maleChildren.length() >= 2) maleChildren[1]->setName("Алекс Рэд");
            if (maleChildren.length() >= 3) maleChildren[2]->setName("Адам Рэд");
            if (femaleChildren.length() >= 1) femaleChildren[0]->setName("Аманда Рэд");
            if (femaleChildren.length() >= 2) femaleChildren[1]->setName("Анна Рэд");
            if (femaleChildren.length() >= 3) femaleChildren[2]->setName("Алиса Рэд");
        }
        if (familyTag == FamilyTag::Wood)
        {
            if (maleAdults.length() >= 1) maleAdults[0]->setName("Мистер Стэнли Вуд");
            if (femaleAdults.length() >= 1) femaleAdults[0]->setName("Миссис Саманта Вуд");
            if (maleChildren.length() >= 1) maleChildren[0]->setName("Джек Вуд");
            if (maleChildren.length() >= 2) maleChildren[1]->setName("Джон Вуд");
            if (maleChildren.length() >= 3) maleChildren[2]->setName("Дон Вуд");
            if (femaleChildren.length() >= 1) femaleChildren[0]->setName("Долорес Вуд");
            if (femaleChildren.length() >= 2) femaleChildren[1]->setName("Джулия Вуд");
            if (femaleChildren.length() >= 3) femaleChildren[2]->setName("Джейн Вуд");
        }
        if (familyTag == FamilyTag::Ostin)
        {
            if (maleAdults.length() >= 1) maleAdults[0]->setName("Мистер Форд Остин");
            if (femaleAdults.length() >= 1) femaleAdults[0]->setName("Миссис Флора Остин");
            if (maleChildren.length() >= 1) maleChildren[0]->setName("Харви Остин");
            if (maleChildren.length() >= 2) maleChildren[1]->setName("Ларри Остин");
            if (maleChildren.length() >= 3) maleChildren[2]->setName("Кевин Остин");
            if (femaleChildren.length() >= 1) femaleChildren[0]->setName("Джейн Остин");
            if (femaleChildren.length() >= 2) femaleChildren[1]->setName("Марта Остин");
            if (femaleChildren.length() >= 3) femaleChildren[2]->setName("Майя Остин");
        }
        if (familyTag == FamilyTag::Black)
        {
            if (maleAdults.length() >= 1) maleAdults[0]->setName("Мистер Гарри Блэк");
            if (femaleAdults.length() >= 1) femaleAdults[0]->setName("Миссис Глория Блэк");
            if (maleChildren.length() >= 1) maleChildren[0]->setName("Майкл Блэк");
            if (maleChildren.length() >= 2) maleChildren[1]->setName("Макс Блэк");
            if (maleChildren.length() >= 3) maleChildren[2]->setName("Мерфи Блэк");
            if (femaleChildren.length() >= 1) femaleChildren[0]->setName("Милли Блэк");
            if (femaleChildren.length() >= 2) femaleChildren[1]->setName("Мойра Блэк");
            if (femaleChildren.length() >= 3) femaleChildren[2]->setName("Молли Блэк");
        }
        if (familyTag == FamilyTag::Smith)
        {
            if (maleAdults.length() >= 1) maleAdults[0]->setName("Мистер Ларри Смит");
            if (femaleAdults.length() >= 1) femaleAdults[0]->setName("Миссис Лилиан Смит");
            if (maleChildren.length() >= 1) maleChildren[0]->setName("Кайл Смит");
            if (maleChildren.length() >= 2) maleChildren[1]->setName("Коди Смит");
            if (maleChildren.length() >= 3) maleChildren[2]->setName("Кевин Смит");
            if (femaleChildren.length() >= 1) femaleChildren[0]->setName("Кэтрин Смит");
            if (femaleChildren.length() >= 2) femaleChildren[1]->setName("Кайра Смит");
            if (femaleChildren.length() >= 3) femaleChildren[2]->setName("Келли Смит");
        }
        if (familyTag == FamilyTag::Li)
        {
            if (maleAdults.length() >= 1) maleAdults[0]->setName("Мистер Сэммюэль Ли");
            if (femaleAdults.length() >= 1) femaleAdults[0]->setName("Миссис Сюзанна Ли");
            if (maleChildren.length() >= 1) maleChildren[0]->setName("Филлип Ли");
            if (maleChildren.length() >= 2) maleChildren[1]->setName("Томас Ли");
            if (maleChildren.length() >= 3) maleChildren[2]->setName("Фред Ли");
            if (femaleChildren.length() >= 1) femaleChildren[0]->setName("Флоранс Ли");
            if (femaleChildren.length() >= 2) femaleChildren[1]->setName("Фанни Ли");
            if (femaleChildren.length() >= 3) femaleChildren[2]->setName("Френсис Ли");
        }
    }
}

void FamiliesHandler::addFamilyDescriptions(Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        FamilyTag familyTag = families[i]->getFamilyTag();
        Members adults = families[i]->get(Tags({Tag::Adult}));
        Members children = families[i]->get(Tags({Tag::Child}));
        if (familyTag == FamilyTag::Red)
        {
            for (int j = 0; j < adults.length(); j++)
                adults[j]->setFamilyDescription("Вы правильная и религиозная семья. Но ваши дети словно посланы Богом вам в наказание! Недавно до вас дошли слухи, что они еще и приторговывают наркотой...");
            for (int j = 0; j < children.length(); j++)
                children[j]->setFamilyDescription("Твои родители тебя достали. Они совсем повернулись на религии и сводят тебя с ума подозрительностью, а последнее время вообще не спускают с тебя глаз. Пусть идут к черту со своими нелепыми правилами!");
        }
        if (familyTag == FamilyTag::Wood)
        {
            for (int j = 0; j < adults.length(); j++)
                adults[j]->setFamilyDescription("Твоя борьба за справедливость не раз заставляла менять города и должности. Потому что лучше быть честным человеком, чем богатым лицемером. Твоя семья тебя всегда поддерживает. И это - главное!");
            for (int j = 0; j < children.length(); j++)
                children[j]->setFamilyDescription("Твои предки - самые крутые, справедливые и честные люди! Вы часто переезжаете, и тебе нравятся новые места и новые люди. Учеба в универе вот-вот закончится, и ты уверен, что будешь много путешествовать!");
        }
        if (familyTag == FamilyTag::Ostin)
        {
            for (int j = 0; j < adults.length(); j++)
                adults[j]->setFamilyDescription("Ваш мотель появился здесь раньше, чем этот город. И все было хорошо... Пока отель не был проигран в казино. А дети не связались с компанией наркоманов...");
            for (int j = 0; j < children.length(); j++)
                children[j]->setFamilyDescription("Ты в шоке от своей семьи! Семейный мотель проигран в казино! А еще говорили, что ТЫ живешь неправильно, ха! По крайней мере, теперь будет  проблема поважней, чем твои друзья, курящие травку.");
        }
        if (familyTag == FamilyTag::Black)
        {
            for (int j = 0; j < adults.length(); j++)
                adults[j]->setFamilyDescription("Вся твоя жизнь была посвящена науке. Без тебя твои дети-бестолочи даже не поступили бы в Университет, ведь на уме у них только пьянки-гулянки. Но никто из твоей семьи этого не оценил.");
            for (int j = 0; j < children.length(); j++)
                children[j]->setFamilyDescription("У твоих родителей все хорошо с мозгами, но полный провал с чувствами. Всю жизнь они думали только о науке и никогда - о тебе. Никто не спросил, чего хочешь ты, когда насильно запихнули в Университет.");
        }
        if (familyTag == FamilyTag::Smith)
        {
            for (int j = 0; j < adults.length(); j++)
                adults[j]->setFamilyDescription("Наконец-то у вашей семьи появилась собственная клиника! Переезд из Канады стоил того. Твои дети были против, и вы сильно поссорились. Но ты уверен, что со временем они поймут и скажут спасибо.");
            for (int j = 0; j < children.length(); j++)
                children[j]->setFamilyDescription("Тебя насильно увезли из Канады ради \"светлого будущего вашей семьи\". Всем плевать, что там у тебя осталась куча друзей и вся твоя жизнь.");
        }
        if (familyTag == FamilyTag::Li)
        {
            for (int j = 0; j < adults.length(); j++)
                adults[j]->setFamilyDescription("Ваш магазин обанкротился, и нужно срочно найти работу. Это не так легко, ты гордый человек, и не хочешь вкалывать за гроши. Горе вас сплотило, и, кажется, дети уже не рвутся уехать в мегаполис.");
            for (int j = 0; j < children.length(); j++)
                children[j]->setFamilyDescription("Магазин твоих родителей обанкротился. Ты хочешь им помочь и тоже найти хоть какую-то подработку. Но, кажется, свои мечты о красивой жизни в большом городе можно похоронить...");
        }
    }
}

void FamiliesHandler::addFamilyStatuses(Families families)
{
    QString status;
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
        {
            if (members[j]->getMaturity() == Tag::Adult)
            {
                if (members[j]->getGender() == Tag::Female) status = "Мать";
                else if (members[j]->getGender() == Tag::Male) status = "Отец";
            }
            else
            {
                if (members[j]->getGender() == Tag::Female) status = "Дочь";
                else if (members[j]->getGender() == Tag::Male) status = "Сын";
            }
            members[j]->setFamilyStatus(status + " в семье " + families[i]->getFamilyName());
        }
    }
}

void FamiliesHandler::debug(Families families)
{
    int peoples = 0;
    for (int i = 0; i < families.length(); i++)
    {
        qDebug().noquote() << '\n' << i + 1 << "-" << "семья" << (families[i]->getFamilyName() + ":");
        qDebug() << "Мужчин:" << families[i]->nGet(Tags({Tag::Male})) << "|"
                 << "Девушек:" << families[i]->nGet(Tags({Tag::Female})) << "|"
                 << "Взрослых мужчин:" << families[i]->nGet(Tags({Tag::Male, Tag::Adult})) << "|"
                 << "Взрослых девушек:" << families[i]->nGet(Tags({Tag::Female, Tag::Adult})) << "|"
                 << "Юных мужчин:" << families[i]->nGet(Tags({Tag::Male, Tag::Child})) << "|"
                 << "Юных девушек:" << families[i]->nGet(Tags({Tag::Female, Tag::Child}));

        Members members = families[i]->getMembers();
        peoples += members.length();
        for (int k = 0; k < members.length(); k++)
        {
            QDebug deb = qDebug().noquote();

            deb << k + 1
                << ":"
                << members[k]->getName()
                << ":"
                << members[k]->getFamilyStatus()
                << ":"
                << members[k]->getLivingPlace()
                << ":"
                << members[k]->getOrganizationPosition()
                << ":";

            Tags tags = members[k]->getTags();
            if (tags.contains(Tag::Male)) deb << "Мужчина" << ":";
            if (tags.contains(Tag::Female)) deb << "Девушка" << ":";
            if (tags.contains(Tag::Child)) deb << "Юный" << ":";
            if (tags.contains(Tag::Adult)) deb << "Взрослый" << ":";
            if (tags.contains(Tag::Married)) deb << "Обручен" << ":";
            if (tags.contains(Tag::FirstPerson)) deb << "Первое лицо" << ":";
            if (tags.contains(Tag::SecondPerson)) deb << "Второе лицо" << ":";
            if (tags.contains(Tag::Beauty)) deb << "Лапочка" << ":";
            if (tags.contains(Tag::Beast)) deb << "Чудовище";

            Roles roles = members[k]->getRoles();
            for (int j = 0; j < roles.length(); j++)
            {
                RoleType roleType = roles[j]->getRoleType();
                QString roleTypeString;
                if (roleType == RoleType::FirstRole) roleTypeString = "+";
                else if (roleType == RoleType::SecondRole) roleTypeString = "-";
                deb << ":" << ("<" + roleTypeString + ">")
                    << roles[j]->getRelationship()->getTitle()
                    << ("(" + roles[j]->getName() + ")");
            }

        }
    }
    qDebug() << "\nЛюдей:" << peoples;
}

