#include "role.h"

Role::Role(Tags tags, Relationship* relationship, QObject* parent)
    : QObject(parent)
{
    this->tags = tags;
    this->relationship = relationship;
}

Tags Role::getTags() { return tags; }
RoleType Role::getRoleType() { return roleType; }
Relationship* Role::getRelationship() { return relationship; }
QString Role::getTitle() { return title; }
QString Role::getDescription() { return description; }
QString Role::getName() { return name; }
Role* Role::getRelation() { return relation; }

void Role::setRelationship(Relationship* relationship) { this->relationship = relationship; }
void Role::setRoleType(RoleType roleType) { this->roleType = roleType; }
void Role::setTitle(QString title) { this->title = title; }
void Role::setDescription(QString description) { this->description = description; }
void Role::setName(QString name) { this->name = name; }
void Role::setRelation(Role* relation) { this->relation = relation; }
