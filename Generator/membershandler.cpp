#include "membershandler.h"

Members MembersHandler::filterByTags(Members members, Tags tags)
{
    Members result;
    for (int i = 0; i < members.length(); i++)
        if (Helpers::containsAll(members[i]->getTags(), tags))
            result.append(members[i]);
    return result;
}
