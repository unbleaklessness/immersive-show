#ifndef H_H
#define H_H

#include <QtGlobal>
#include <QtAlgorithms>
#include <QtDebug>

class Helpers
{
public:
    static int randInt(int low, int high);
    template <class T> static bool containsAll(QVector<T> first, QVector<T> second);
    template <class T> static int max(QVector<T> vector);
    template <class T> static int min(QVector<T> vector);
    template <class T> static QVector<T*> shuffle(QVector<T*> vector);
};

template <class T> bool Helpers::containsAll(QVector<T> first, QVector<T> second)
{
    int counter = 0;
    for (int i = 0; i < second.length(); i++)
    {
        if (first.contains(second[i]))
            counter += 1;
    }
    if (counter == second.length())
        return true;
    else
        return false;
}

template <class T> int Helpers::max(QVector<T> vector)
{
    int m = 0;
    for (int i = 0; i < vector.length(); i++)
        if (vector[i] > m) m = vector[i];
    return m;
}

template <class T> int Helpers::min(QVector<T> vector)
{
    int m = vector[0];
    for (int i = 0; i < vector.length(); i++)
        if (vector[i] < m) m = vector[i];
    return m;
}

template <class T> QVector<T*> Helpers::shuffle(QVector<T*> vector)
{
    for (int i = vector.length() - 1; i > 0; i--)
    {
        int j = Helpers::randInt(0, i);
        T* temp = vector[i];
        vector[i] = vector[j];
        vector[j] = temp;
    }
    return vector;
}

#endif // H_H
