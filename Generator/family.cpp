#include "family.h"

Family::Family(FamilyTag familyTag, QObject* parent)
    : QObject(parent)
{
    this->familyTag = familyTag;
}

void Family::addMember(Tags tags)
{
    Member* member = new Member(this, tags, this);
    members.append(member);
}

void Family::addMember(Member* member)
{
    member->setFamily(this);
    members.append(member);
}

void Family::setMembers(Members members) { this->members = members; }
void Family::setInfo(QString info) { this->info = info; }
void Family::setFamilyName(QString familyName) { this->familyName = familyName; }

Member* Family::getFirstPerson()
{
    Members maleAdults = get(Tags({Tag::Adult, Tag::Male}));
    Members femaleAdults = get(Tags({Tag::Adult, Tag::Female}));

    if (maleAdults.length() != 0)
        return maleAdults[0];
    if (femaleAdults.length() != 0)
        return femaleAdults[0];

    throw std::runtime_error("There is no first person, but you have called getFirstPerson() method!");
}

Member* Family::getSecondPerson()
{
    Members maleAdults = get(Tags({Tag::Adult, Tag::Male}));
    Members femaleAdults = get(Tags({Tag::Female, Tag::Adult}));

    if (femaleAdults.length() != 0 && maleAdults.length() != 0)
        return femaleAdults[0];

    throw std::runtime_error("There is no second person, but you have called getSecondPerson() method!");
}

Members Family::getMembers() { return members; }
QString Family::getFamilyName() { return familyName; }
QString Family::getInfo() { return info; }
Members Family::get(Tags tags) { return MembersHandler::filterByTags(members, tags); }
FamilyTag Family::getFamilyTag() { return familyTag; }

bool Family::firstPersonExists()
{
    int maleAdults = nGet(Tags({Tag::Adult, Tag::Male}));
    int femaleAdults = nGet(Tags({Tag::Adult, Tag::Female}));

    if (maleAdults != 0 || femaleAdults != 0) return true;
    return false;
}

bool Family::secondPersonExists()
{
    int maleAdults = nGet(Tags({Tag::Adult, Tag::Male}));
    int femaleAdults = nGet(Tags({Tag::Adult, Tag::Female}));

    if (maleAdults != 0 && femaleAdults != 0) return true;
    return false;
}

int Family::nGet(Tags tags) { return get(tags).length(); }
int Family::nMembers() { return members.length(); }
