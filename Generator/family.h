#ifndef FAMILY_H
#define FAMILY_H

#include <QObject>
#include <QVector>

#include "member.h"
#include "tag.h"
#include "helpers.h"
#include "membershandler.h"
#include "familytag.h"

class Member;
enum class Tag;
typedef QVector<Member*> Members;

class Family : public QObject
{
    Q_OBJECT

private:
    QString familyName;
    QString info;
    Members members;
    FamilyTag familyTag;

public:
    explicit Family(FamilyTag familyTag, QObject* parent = nullptr);

    void addMember(Tags tags);
    void addMember(Member* member);
    void setMembers(Members members);
    void setInfo(QString info);
    void setFamilyName(QString familyName);

    QString getFamilyName();
    QString getInfo();
    Members getMembers();
    Members get(Tags tags);
    Member* getFirstPerson();
    Member* getSecondPerson();
    FamilyTag getFamilyTag();

    bool firstPersonExists();
    bool secondPersonExists();

    int nGet(Tags tags);
    int nMembers();
};

typedef QVector<Family*> Families;

#endif // FAMILY_H
