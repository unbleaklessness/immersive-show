#ifndef ROLETYPE_H
#define ROLETYPE_H

enum class RoleType
{
    FirstRole, SecondRole
};

#endif // ROLETYPE_H
