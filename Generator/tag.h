#ifndef TAGS_H
#define TAGS_H

#include <QVector>

enum class Tag
{
    Male, Female, Child, Adult, Beast, Beauty, NotAddict, None,
    FirstPerson, SecondPerson, Married, Student, Worker, Volunteer
};

typedef QVector<Tag> Tags;

#endif // TAGS_H
