#include "rolefactory.h"

RoleFactory::RoleFactory(Relationship* relationship)
{
    this->relationship = relationship;
}

Role* RoleFactory::getFirstRole(Tags tags)
{
    Role* firstRole = new Role(tags, relationship);
    firstRole->setRoleType(RoleType::FirstRole);
    return firstRole;
}

Role* RoleFactory::getSecondRole(Tags tags)
{
    Role* secondRole = new Role(tags, relationship);
    secondRole->setRoleType(RoleType::SecondRole);
    return secondRole;
}
