#ifndef RELATIONSHIPS_FACTORY_H
#define RELATIONSHIPS_FACTORY_H

#include <QObject>

#include "helpers.h"
#include "relationship.h"
#include "rolefactory.h"

class RelationshipFactory : public QObject
{
    Q_OBJECT

private:
    void addRelationshipIndexes(Relationships relationships);

public:
    explicit RelationshipFactory(QObject* parent = nullptr);

    Relationship* getRomeoAndJuliet();
    Relationship* getBeautyAndTheBeast();
    Relationship* getOneginAndTatiana();
    Relationship* getSnowWhite();
    Relationship* getAdultBetrayal();
    Relationship* getSnowWhiteReversed();
    Relationship* getYoungAndPregnant();
    Relationship* getFemaleAdultsEnvy();
    Relationship* getYoungFemalesVengence();
    Relationship* getAdoptedSon();
    Relationship* getTwoSisters();
    Relationship* getAdoptedDaughter();
    Relationship* getNewFire();
    Relationship* getBigGame();
    Relationship* getFearOfLose();
    Relationship* getJealous();
    Relationship* getAdultAndPregnant();
    Relationship* getFreaks1();
    Relationship* getFreaks2();
    Relationship* getBadVolunteer();
    Relationship* getGoodVolunteer();

    Relationships getRelationships();

};

#endif // RELATIONSHIPS_FACTORY_H
