#include "relationshipsdistributor.h"

void RelationshipsDistributor::distribute(Relationships relationships, Families families)
{
    QVector<int> indexesEmptyMembers;
    QVector<int> indexesTwoRoles;

    QVector<Families> tempFamilies;
    QVector<Relationships> tempRelationships;

    int maxIterations = 3000;
    for (int iterator = 0; iterator < maxIterations; iterator++)
    {
        distributeRoleGroups(relationships, families);

        indexesEmptyMembers.append(countMembersWithMaxNRoles(families, 0));
        indexesTwoRoles.append(countMembersWithMinNRoles(families, 2));

        tempFamilies.append(families);
        tempRelationships.append(relationships);

        removeRoles(families);

        families = Helpers::shuffle(families);
        for (int i = 0; i < families.length(); i++)
            families[i]->setMembers(Helpers::shuffle(families[i]->getMembers()));

        relationships = Helpers::shuffle(relationships);
        for (int i = 0; i < relationships.length(); i++)
        {
            RoleGroups roleGroups = relationships[i]->getRoleGroups();
            for (int j = 0; j < roleGroups.length(); j++)
                roleGroups[j] = Helpers::shuffle(roleGroups[j]);
            relationships[i]->setRoleGroups(roleGroups);
        }
    }

    QVector<int> difference;
    for (int i = 0; i < maxIterations; i++)
    {
        if (indexesEmptyMembers[i] != 0) difference.append(0);
        else difference.append(indexesTwoRoles[i]);
    }

    int best = difference.indexOf(Helpers::max(difference));

    relationships = tempRelationships[best];
    families = tempFamilies[best];

    distributeRoleGroups(relationships, families);
    addRelatedMembers(relationships, families);
}

void RelationshipsDistributor::distributeRoleGroups(Relationships relationships, Families families)
{
    for (int i = 0; i < relationships.length(); i++)
    {
        RoleGroups roleGroups = relationships[i]->getRoleGroups();
        for (int j = 0; j < roleGroups.length(); j++)
            distributeRoleGroup(roleGroups[j], families);
        if (isRelationshipIncomplete(relationships[i], families))
            removeRelationship(relationships[i], families);
    }
}

int RelationshipsDistributor::countMembersWithMinNRoles(Families families, int n)
{
    int counter = 0;
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
            if (members[j]->getRoles().length() >= n) counter++;
    }
    return counter;
}

int RelationshipsDistributor::countMembersWithMaxNRoles(Families families, int n)
{
    int counter = 0;
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
            if (members[j]->getRoles().length() <= n) counter++;
    }
    return counter;
}

void RelationshipsDistributor::removeRoles(Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
            members[j]->setRoles(Roles({}));
    }
}

bool RelationshipsDistributor::isMemberFitsInRole(Member* member, Role* role)
{
    RoleType roleType = role->getRoleType();
    int firstRoles = RolesHandler::nRoleType(member->getRoles(), RoleType::FirstRole);
    int secondRoles = RolesHandler::nRoleType(member->getRoles(), RoleType::SecondRole);
    if (Helpers::containsAll(member->getTags(), role->getTags()) &&
            ((roleType == RoleType::FirstRole && firstRoles < 1) ||
            (roleType == RoleType::SecondRole && secondRoles < 2))) return true;
    return false;
}

int RelationshipsDistributor::nRolesFitsInFamily(Roles roles, Family* family)
{
    int counter = 0;
    QVector<int> used;
    Members members = family->getMembers();
    for (int i = 0; i < roles.length(); i++)
        for (int j = 0; j < members.length(); j++)
            if (isMemberFitsInRole(members[j], roles[i]) && !used.contains(j))
            {
                counter += 1;
                used.append(j);
                break;
            }
    return counter;
}

bool RelationshipsDistributor::isRelationshipInFamily(Relationship* relationship, Family* family)
{
    Members members = family->getMembers();
    for (int i = 0; i < members.length(); i++)
    {
        Roles roles = members[i]->getRoles();
        for (int j = 0; j < roles.length(); j++)
            if (roles[j]->getRelationship() == relationship) return true;
    }
    return false;
}

int RelationshipsDistributor::iBestFamilyForRoleGroup(Roles roles, Families families)
{
    QVector<int> fits;
    for (int i = 0; i < families.length(); i++)
    {
        Relationship* relationship = roles[0]->getRelationship();
        if (isRelationshipInFamily(relationship, families[i]))
        {
            fits.append(-1);
            continue;
        }
        int n = nRolesFitsInFamily(roles, families[i]);
        fits.append(n);
    }
    int best = fits.indexOf(Helpers::max(fits));
    return best;
}

void RelationshipsDistributor::distributeRoleGroup(Roles roles, Families families)
{
    int best = iBestFamilyForRoleGroup(roles, families);
    if (best != -1)
        addRoleGroupToFamily(roles, families[best]);
}

void RelationshipsDistributor::addRoleGroupToFamily(Roles roles, Family* family)
{
    QVector<int> used;
    Members members = family->getMembers();
    for (int j = 0; j < roles.length(); j++)
        for (int i = 0; i < members.length(); i++)
            if (isMemberFitsInRole(members[i], roles[j]) && !used.contains(i))
            {
                members[i]->addRole(roles[j]);
                used.append(i);
                break;
            }
    family->setMembers(members);
}

bool RelationshipsDistributor::isRelationshipIncomplete(Relationship* relationship, Families families)
{
    int firstRoles = 0;
    int secondRoles = 0;
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
        {
            Roles roles = members[j]->getRoles();
            for (int k = 0; k < roles.length(); k++)
            {
                if (!(roles[k]->getRelationship() == relationship)) continue;
                if (roles[k]->getRoleType() == RoleType::FirstRole) firstRoles++;
                else if (roles[k]->getRoleType() == RoleType::SecondRole) secondRoles++;
            }
        }
    }
    if (firstRoles <= 1 || secondRoles == 0) return true;
    return false;
}

void RelationshipsDistributor::removeRelationship(Relationship* relationship, Families families)
{
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
        {
            Roles roles = members[j]->getRoles();
            for (int k = 0; k < roles.length(); k++)
                if (roles[k]->getRelationship() == relationship)
                    roles.remove(k);
            members[j]->setRoles(roles);
        }
        families[i]->setMembers(members);
    }
}

Members RelationshipsDistributor::findRoleInFamilies(Role* role, Families families)
{
    Members result;
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
            if (members[j]->getRoles().contains(role)) result.append(members[j]);
    }
    return result;
}

void RelationshipsDistributor::addRelatedMembers(Relationships relationships, Families families)
{
    for (int i = 0; i < relationships.length(); i++)
    {
        RoleGroups relations = relationships[i]->getRelations();
        for (int j = 0; j < relations.length(); j++)
        {
            Members first = findRoleInFamilies(relations[j][0], families);
            Members second = findRoleInFamilies(relations[j][1], families);
            if (!first.empty() && !second.empty())
                first[0]->addRelationInfo(second[0]->getName(), relations[j][0]->getDescription());
        }
    }
}
