#ifndef FAMILY_MEMBER_H
#define FAMILY_MEMBER_H

#include <QObject>
#include <QHash>

#include "family.h"
#include "role.h"
#include "tag.h"
#include "roletype.h"

class Family;
class Role;
typedef QVector<Role*> Roles;

class Member : public QObject
{
    Q_OBJECT

private:
    QString name;
    QString familyStatus;
    QString livingPlace;
    QString morningRoutine;
    QString dayRoutine;
    QString eveningRoutine;
    QString familyDescription;
    Tags tags;
    Family* family;
    QString organization;
    QString organizationDescription;
    QString organizationPosition;
    int organizationIndex = -1;
    Roles roles;
    QHash<QString, QString> relationsInfo;

public:
    explicit Member(Family* family, Tags tags, QObject* parent = nullptr);

    QString getName();
    QString getFamilyStatus();
    QString getLivingPlace();
    QString getMorningRoutine();
    QString getDayRoutine();
    QString getEveningRoutine();
    QString getFamilyDescription();
    Family* getFamily();
    Tag getMaturity();
    Tag getGender();
    Tags getTags();
    QString getOrganization();
    QString getOrganizationDescription();
    QString getOrganizationPosition();
    int getOrganizationIndex();
    Roles getRoles();
    QHash<QString, QString> getRelationsInfo();

    void setName(QString name);
    void setFamilyStatus(QString familyStatus);
    void setLivingPlace(QString livingPlace);
    void setMorningRoutine(QString morningRoutine);
    void setDayRoutine(QString dayRoutine);
    void setEveningRoutine(QString eveningRoutine);
    void setFamilyDescription(QString familyDescription);
    void setFamily(Family* family);
    void setOrganization(QString organization);
    void setOrganizationDescription(QString description);
    void setOrganizationPosition(QString position);
    void setOrganizationIndex(int index);
    void setRoles(Roles roles);
    void setRelationsInfo(QHash<QString, QString> relationsInfo);
    void addTag(Tag tag);
    void addRole(Role* role);
    void addRelationInfo(QString name, QString description);
};

typedef QVector<Member*> Members;

#endif // FAMILY_MEMBER_H
