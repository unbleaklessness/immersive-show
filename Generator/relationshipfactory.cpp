#include "relationshipfactory.h"

RelationshipFactory::RelationshipFactory(QObject* parent) : QObject(parent) {}

Relationship* RelationshipFactory::getRomeoAndJuliet()
{
    Relationship* romeoAndJuliet = new Relationship("Ромео и Джульетта");
    RoleFactory* rolesFactory = new RoleFactory(romeoAndJuliet);

    Role* romeo = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Child}));
    romeo->setName("Ромео");
    romeo->setDescription("Ты встречаешься с самой обалденной девушкой во всем городе! У вас все круто, и она тебя обожает. Но есть одна проблема - ваши предки против. Мягко сказать. Они вообще друг друга презирают и ненавидят. Вы пытались скрывать свои отношения, но всё это вылилось в жуткий скандал.");
    romeoAndJuliet->addRole(romeo);

    Role* romeoFirstParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    romeoFirstParent->setName("Первый родитель Ромео");
    romeoFirstParent->setDescription("Твой сын влюбился в ту, чью семью ты открыто презираешь. Ты никогда не примешь его выбор.");
    romeoAndJuliet->addRole(romeoFirstParent);

    Role* romeoSecondParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    romeoSecondParent->setName("Второй родитель Ромео");
    romeoSecondParent->setDescription("Твой сын влюбился в ту, чью семью ты открыто презираешь. Ты никогда не примешь его выбор.");
    romeoAndJuliet->addRole(romeoSecondParent);

    Role* juliet = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    juliet->setName("Джульетта");
    juliet->setDescription("Ты встречаешься с самым крутым парнем во всем городе! У вас все круто, и он тебя обожает. Но есть одна проблема - ваши предки против. Мягко сказано! Они вообще друг друга презирают и ненавидят. Вы пытались скрывать свои отношения, но всё это вылилось в жуткий скандал.");
    romeoAndJuliet->addRole(juliet);

    Role* julietFirstParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    julietFirstParent->setName("Первый родитель Джульетты");
    julietFirstParent->setDescription("Твоя дочь влюбилась в того, чью семью ты открыто презираешь. Ты никогда не примешь ее выбор.");
    romeoAndJuliet->addRole(julietFirstParent);

    Role* julietSecondParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    julietSecondParent->setName("Второй родитель Джульетты");
    julietSecondParent->setDescription("Твоя дочь влюбилась в того, чью семью ты открыто презираешь. Ты никогда не примешь ее выбор.");
    romeoAndJuliet->addRole(julietSecondParent);

    romeoAndJuliet->addRoleGroup(Roles({romeo, romeoFirstParent, romeoSecondParent}));
    romeoAndJuliet->addRoleGroup(Roles({juliet, julietFirstParent, julietSecondParent}));

    romeoAndJuliet->addRelation(Roles({juliet, romeo}));
    romeoAndJuliet->addRelation(Roles({julietFirstParent, juliet}));
    romeoAndJuliet->addRelation(Roles({julietSecondParent, juliet}));
    romeoAndJuliet->addRelation(Roles({romeo, juliet}));
    romeoAndJuliet->addRelation(Roles({romeoFirstParent, romeo}));
    romeoAndJuliet->addRelation(Roles({romeoSecondParent, romeo}));

    return romeoAndJuliet;
}

Relationship* RelationshipFactory::getBeautyAndTheBeast()
{
    Relationship* beautyAndTheBeast = new Relationship("Красавица и Чудовище");
    RoleFactory* rolesFactory = new RoleFactory(beautyAndTheBeast);

    Role* beast = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Child, Tag::Beast}));
    beast->setName("Чудовище");
    beast->setDescription("Тебя угораздило влюбиться в пай-девочку. Она вся такая чистая и наивная... В общем, совсем не такая, как ты. Но тем не менее вы вместе. Хотя  ваши попытки переделать друг друга изрядно бесят тебя и твоих друзей. Они вообще не понимают, что ты в ней нашел.");
    beautyAndTheBeast->addRole(beast);

    Role* beastParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    beastParent->setName("Родитель Чудовища");
    beastParent->setDescription("Твой ребенок давно отбился от рук. Но, может, эта любовь поставит его на путь исправления?");
    beautyAndTheBeast->addRole(beastParent);

    Role* beastFriend = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Child}));
    beastFriend->setName("Друг Чудовища");
    beastFriend->setDescription("Твой друг сбрендил от любви! Нафиг ему нужна эта клуша? Еще и командует. Пора сказать ему об этом.");
    beautyAndTheBeast->addRole(beastFriend);

    Role* beauty = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    beauty->setName("Красавица");
    beauty->setDescription("Твой парень - настоящий хулиган, разве что не уголовник. В общем, совсем не такой, как ты. Но тем не менее вы вместе. Хотя  ваши попытки переделать друг друга изрядно бесят тебя и твоих друзей. Они вообще не понимают, что ты в нем нашла.");
    beautyAndTheBeast->addRole(beauty);

    Role* beautyParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    beautyParent->setName("Родитель Красавицы");
    beautyParent->setDescription("Твоя дочка связалась с негодяем. Говорят, он даже сидит на наркоте...Ты очень беспокоишься за нее.");
    beautyAndTheBeast->addRole(beautyParent);

    Role* beautyFriend = rolesFactory->getSecondRole(Tags({Tag::Female, Tag::Child}));
    beautyFriend->setName("Друг Красавицы");
    beautyFriend->setDescription("Твоя подруга сбрендила от любви! Ее парень дает ей наркотики! Ты хочешь открыть ей глаза.");
    beautyAndTheBeast->addRole(beautyFriend);

    beautyAndTheBeast->addRoleGroup(Roles({beast, beastParent}));
    beautyAndTheBeast->addRoleGroup(Roles({beauty, beautyParent}));
    beautyAndTheBeast->addRoleGroup(Roles({beastFriend}));
    beautyAndTheBeast->addRoleGroup(Roles({beautyFriend}));

    beautyAndTheBeast->addRelation(Roles({beast, beauty}));
    beautyAndTheBeast->addRelation(Roles({beastFriend, beast}));
    beautyAndTheBeast->addRelation(Roles({beastParent, beast}));
    beautyAndTheBeast->addRelation(Roles({beauty, beast}));
    beautyAndTheBeast->addRelation(Roles({beautyParent, beauty}));
    beautyAndTheBeast->addRelation(Roles({beautyFriend, beauty}));

    return beautyAndTheBeast;
}

Relationship* RelationshipFactory::getOneginAndTatiana()
{
    Relationship* oneginAndTatiana = new Relationship("Онегин и Татьяна");
    RoleFactory* rolesFactory = new RoleFactory(oneginAndTatiana);

    Role* tatiana = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Adult, Tag::Married}));
    tatiana->setName("Татьяна");
    tatiana->setDescription("На днях ты встретилась с любовью своей юности. Даже страшно вспомнить, как ты за ним бегала, а он тебя отверг. Ты была на него зла. И вот сейчас, когда ты уже замужем, ему взбрело в голову влюбиться в тебя. Неужели ты правда к нему ничего не чувствуешь?");
    oneginAndTatiana->addRole(tatiana);

    Role* tatianaHusband = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Adult, Tag::Married, Tag::Beast}));
    tatianaHusband->setName("Муж Татьяны");
    tatianaHusband->setDescription("Все было хорошо, но на горизонте появился ее бывший. И вроде повода для ревности нет, но...");
    oneginAndTatiana->addRole(tatianaHusband);

    Role* tatianaChild = rolesFactory->getSecondRole(Tags({Tag::Child}));
    tatianaChild->setName("Ребенок Татьяны");
    tatianaChild->setDescription("Твоя мама в последнее время сама не своя. Кажется, у нее проблемы с отцом. Надо разобраться в этом.");
    oneginAndTatiana->addRole(tatianaChild);

    Role* onegin = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Adult}));
    onegin->setName("Онегин");
    onegin->setDescription("Каким ты был дураком, что отверг ее тогда, в юности, когда она сама вешалась тебе на шею! Теперь она из нескладного подростка превратилась в красивую  женщину, она замужем, ее любят и уважают. А ты... недавно вновь встретил ее и влюбился, как мальчишка. И хочешь добиться ее во что бы то ни стало!");
    oneginAndTatiana->addRole(onegin);

    Role* oneginFriend = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Adult}));
    oneginFriend->setName("Друг Онегина");
    oneginFriend->setDescription("Твой друг страдает. Ты не знаешь почему, но тебе невыносимо на это смотреть. Надо ему как-то помочь.");
    oneginAndTatiana->addRole(oneginFriend);

    oneginAndTatiana->addRoleGroup(Roles({tatiana, tatianaHusband}));
    oneginAndTatiana->addRoleGroup(Roles({onegin}));
    oneginAndTatiana->addRoleGroup(Roles({oneginFriend}));

    oneginAndTatiana->addRelation(Roles({tatiana, onegin}));
    oneginAndTatiana->addRelation(Roles({onegin, tatiana}));
    oneginAndTatiana->addRelation(Roles({oneginFriend, onegin}));
    oneginAndTatiana->addRelation(Roles({tatianaHusband, tatiana}));

    return oneginAndTatiana;
}

Relationship* RelationshipFactory::getSnowWhite()
{
    Relationship* snowWhite = new Relationship("Белоснежка");
    RoleFactory* rolesFactory = new RoleFactory(snowWhite);

    Role* snow = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child, Tag::Beast}));
    snow->setName("Белоснежка");
    snow->setDescription("И другие... Ты - очень красивая девушка, внимание мужчин нужно тебе как воздух! Каждый второй для тебя - принц на белом коне. Ты влюбляешься совершенно искренне и готова отдать ему все... Пока не встретишь следующего. Тебе нет дела до предрассудков и сплетен. Ты - самая счастливая женщина в этом городе.");
    snowWhite->addRole(snow);

    Role* snowFirstParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    snowFirstParent->setName("Первый родитель белоснежки");
    snowFirstParent->setDescription("Ты просишь свою дочь образумиться, выбрать, наконец, одного мужчину из ста и не позорить вашу семью.");
    snowWhite->addRole(snowFirstParent);

    Role* snowSecondParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    snowSecondParent->setName("Второй родитель Белоснежки");
    snowSecondParent->setDescription("У твоей дочери хоровод поклонников. Ты не против, лишь бы была осторожна и не опозорилась.");
    snowWhite->addRole(snowSecondParent);

    Role* lover1 = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Child, Tag::Beast}));
    lover1->setName("Первый любовник");
    lover1->setDescription("О таких девушках пишет плейбой. Ты патологически влюблен, она отвечает взаимностью... но не только тебе. И тебя это бесит. Ты не намерен делить ее ни с кем. Ты сделаешь все, чтобы заполучить ее на этот вечер, а потом на следующий. В идеале - навсегда. Но она не знает такого слова.");
    snowWhite->addRole(lover1);

    Role* lover2 = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Child}));
    lover2->setName("Второй любовник");
    lover2->setDescription("Вам круто вдвоем, она - лучшая, и не парит тебе мозги ревностью. И ты ей тоже. Полная свобода!");
    snowWhite->addRole(lover2);

    Role* lover3 = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Adult, Tag::Married, Tag::Beast}));
    lover3->setName("Третий любовник");
    lover3->setDescription("Вам круто вдвоем, и ей не нужно замуж, детей и семью. И тебе - ведь ты уже давно и счастливо женат.");
    snowWhite->addRole(lover3);

    snowWhite->addRoleGroup(Roles({snow, snowFirstParent, snowSecondParent}));
    snowWhite->addRoleGroup(Roles({lover1}));
    snowWhite->addRoleGroup(Roles({lover2}));
    snowWhite->addRoleGroup(Roles({lover3}));

    snowWhite->addRelation(Roles({snow, lover1}));
    snowWhite->addRelation(Roles({snowFirstParent, snow}));
    snowWhite->addRelation(Roles({snowSecondParent, snow}));
    snowWhite->addRelation(Roles({lover1, snow}));
    snowWhite->addRelation(Roles({lover2, snow}));
    snowWhite->addRelation(Roles({lover3, snow}));

    return snowWhite;
}

Relationship* RelationshipFactory::getAdultBetrayal()
{
    Relationship* adultBetrayal = new Relationship("Измена взрослые");
    RoleFactory* rolesFactory = new RoleFactory(adultBetrayal);

    Role* maleAdultBeast = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Adult, Tag::Beast}));
    maleAdultBeast->setName("Изменьщик");
    maleAdultBeast->setDescription("Ты даже не думал, что можешь так влюбиться и потерять голову. Ты встретил девушку, с которой было так легко и кайфово... Пока ты не осознал, какую боль причиняешь этим своей жене.");
    adultBetrayal->addRole(maleAdultBeast);

    Role* femaleAdultBeauty = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Adult, Tag::Married}));
    femaleAdultBeauty->setName("Жертва измены");
    femaleAdultBeauty->setDescription("Тебе казалось, что у вас все хорошо. Но недавно ты поняла, что у него есть другая. Что делать? Развод? Но вас так много связывает...может, он одумается? Ты никогда не думала, что попадешь в подобную ситуацию...");
    adultBetrayal->addRole(femaleAdultBeauty);

    Role* femaleChild = rolesFactory->getSecondRole(Tags({Tag::Female, Tag::Child}));
    femaleChild->setName("С кем изменяют");
    femaleChild->setDescription("Ты в шоке от того, что у него есть жена и дети. Ты не хочешь разрушать семью, но так его любишь...");
    adultBetrayal->addRole(femaleChild);

    Role* maleAdult = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Adult}));
    maleAdult->setName("Сочувствующий");
    maleAdult->setDescription("Вы дружите с детства, она тебе как сестра. Ты видишь, что у нее проблемы в семье, и хочешь помочь.");
    adultBetrayal->addRole(maleAdult);

    Role* childMale = rolesFactory->getSecondRole(Tags({Tag::Child, Tag::Male}));
    childMale->setName("Ребенок-девочка");
    childMale->setDescription("Твоя мама в последнее время сама не своя. Кажется, у нее проблемы с отцом. Надо разобраться в этом.");
    adultBetrayal->addRole(childMale);

    Role* childFemale = rolesFactory->getSecondRole(Tags({Tag::Female, Tag::Child}));
    childFemale->setName("Ребенок-мальчик");
    childFemale->setDescription("Твой отец в последнее время сам не свой. Кажется, он изменяет матери. Надо разобраться в этом.");
    adultBetrayal->addRole(childFemale);

    adultBetrayal->addRoleGroup(Roles({maleAdultBeast, femaleAdultBeauty, childMale, childFemale}));
    adultBetrayal->addRoleGroup(Roles({femaleChild}));
    adultBetrayal->addRoleGroup(Roles({maleAdult}));

    adultBetrayal->addRelation(Roles({maleAdultBeast, femaleAdultBeauty}));
    adultBetrayal->addRelation(Roles({femaleAdultBeauty, maleAdultBeast}));
    adultBetrayal->addRelation(Roles({femaleChild, maleAdultBeast}));
    adultBetrayal->addRelation(Roles({childMale, maleAdultBeast}));
    adultBetrayal->addRelation(Roles({childFemale, femaleAdultBeauty}));
    adultBetrayal->addRelation(Roles({maleAdult, femaleAdultBeauty}));

    return adultBetrayal;
}

Relationship* RelationshipFactory::getSnowWhiteReversed()
{
    Relationship* snowWhiteReversed = new Relationship("Белоснежка наоборот");
    RoleFactory* rolesFactory = new RoleFactory(snowWhiteReversed);

    Role* maleChildBeast = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Child, Tag::Beast}));
    maleChildBeast->setName("Белоснег");
    maleChildBeast->setDescription("И другие... Ты обожаешь женщин! Они такие нежные, яркие, будоражащие. В каждую ты влюбляешься совершенно искренне! Пока не встретишь следующую. Твоя жизнь полна страсти, и ты с радостью делишься ею с женщинами.");
    snowWhiteReversed->addRole(maleChildBeast);

    Role* femaleChildBeauty1 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    femaleChildBeauty1->setName("Первая любовница");
    femaleChildBeauty1->setDescription("Ты никогда не думала, что сможешь так влюбиться! Он просто идеальный. Есть только одно Но. Ты у него не единственная.. и никогда ею не будешь. Так он считает. Но ты умнее. Ты решишь эту проблему по-своему, без соплей и банальной ревности.");
    snowWhiteReversed->addRole(femaleChildBeauty1);

    Role* adult = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    adult->setName("Родитель Белоснега");
    adult->setDescription("Твой сын разбивает женские сердца. Каждый день у него новая девушка, и все влюблены в него по уши.");
    snowWhiteReversed->addRole(adult);

    Role* femaleChildBeauty2 = rolesFactory->getSecondRole(Tags({Tag::Female, Tag::Child}));
    femaleChildBeauty2->setName("Мстительная");
    femaleChildBeauty2->setDescription("Ты любила его одного, а он еще пару девушек. Ты отомстишь ему, и весь город будет над ним смеяться.");
    snowWhiteReversed->addRole(femaleChildBeauty2);

    Role* maleChildBeauty = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Child}));
    maleChildBeauty->setName("Тихоня");
    maleChildBeauty->setDescription("Твой лучший друг пикапер и тусовщик. Ты - полная противоположность, но иногда завидуешь его драйву.");
    snowWhiteReversed->addRole(maleChildBeauty);

    Role* femaleChildBeauty3 = rolesFactory->getSecondRole(Tags({Tag::Female, Tag::Child}));
    femaleChildBeauty3->setName("Вторая любовница");
    femaleChildBeauty3->setDescription("Вам круто вдвоем, и он не парит тебе мозги ревностью. И ты ему тоже. Полная свобода!");
    snowWhiteReversed->addRole(femaleChildBeauty3);

    snowWhiteReversed->addRoleGroup(Roles({maleChildBeast, adult}));
    snowWhiteReversed->addRoleGroup(Roles({femaleChildBeauty1}));
    snowWhiteReversed->addRoleGroup(Roles({femaleChildBeauty2}));
    snowWhiteReversed->addRoleGroup(Roles({femaleChildBeauty3}));
    snowWhiteReversed->addRoleGroup(Roles({maleChildBeauty}));

    snowWhiteReversed->addRelation(Roles({maleChildBeast, femaleChildBeauty1}));
    snowWhiteReversed->addRelation(Roles({femaleChildBeauty1, maleChildBeast}));
    snowWhiteReversed->addRelation(Roles({femaleChildBeauty2, maleChildBeast}));
    snowWhiteReversed->addRelation(Roles({femaleChildBeauty3, maleChildBeast}));
    snowWhiteReversed->addRelation(Roles({adult, maleChildBeast}));
    snowWhiteReversed->addRelation(Roles({maleChildBeauty, maleChildBeast}));

    return snowWhiteReversed;
}

Relationship* RelationshipFactory::getYoungAndPregnant()
{
    Relationship* youngAndPregnant = new Relationship("Юные с залетом");
    RoleFactory* rolesFactory = new RoleFactory(youngAndPregnant);

    Role* boy = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Child, Tag::Beast}));
    boy->setName("Парень");
    boy->setDescription("Вы встречаетесь давно, ты очень любишь ее. Но она призналась, что беременна. Вам по 17, и это проблема. Вы сильно поругались...Тебе нужен совет отца, но как ему рассказать?");
    youngAndPregnant->addRole(boy);

    Role* girl = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    girl->setName("Девушка");
    girl->setDescription("Вы встречаетесь давно, очень любите друг друга. Но вчера ты узнала, что беременна, и рассказала ему. Вы ужасно поссорились...Вам по 17, и это проблема. Родители пока не в курсе. Надо что-то делать...");
    youngAndPregnant->addRole(girl);

    Role* boyFirstParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    boyFirstParent->setName("Первый родитель парня");
    boyFirstParent->setDescription("Твой сын взрослеет. И ты чувствуешь, что скоро начнутся недетские проблемы.");
    youngAndPregnant->addRole(boyFirstParent);

    Role* girlFirstParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    girlFirstParent->setName("Первый родитель девушки");
    girlFirstParent->setDescription("По поведению дочери ты замечаешь, что у нее проблемы. Надо разобраться и помочь.");
    youngAndPregnant->addRole(girlFirstParent);

    Role* boySecondParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    boySecondParent->setName("Второй родитель парня");
    boySecondParent->setDescription("Ты считаешь, что любовь и прочие глупости могут помешать твоему сыну получить образование.");
    youngAndPregnant->addRole(boySecondParent);

    Role* girlSecondParent = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    girlSecondParent->setName("Второй родитель девушки");
    girlSecondParent->setDescription("Твоя дочь взрослеет. И ты чувствуешь, что скоро начнутся недетские проблемы.");
    youngAndPregnant->addRole(girlSecondParent);

    youngAndPregnant->addRoleGroup(Roles({boy, boyFirstParent, boySecondParent}));
    youngAndPregnant->addRoleGroup(Roles({girl, girlFirstParent, girlSecondParent}));

    youngAndPregnant->addRelation(Roles({boy, girl}));
    youngAndPregnant->addRelation(Roles({girl, boy}));
    youngAndPregnant->addRelation(Roles({boyFirstParent, boy}));
    youngAndPregnant->addRelation(Roles({boySecondParent, boy}));
    youngAndPregnant->addRelation(Roles({girlFirstParent, girl}));
    youngAndPregnant->addRelation(Roles({girlSecondParent, girl}));

    return youngAndPregnant;
}

Relationship* RelationshipFactory::getFemaleAdultsEnvy()
{
    Relationship* relationship = new Relationship("Зависть взрослые женщины");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Adult, Tag::Beast}));
    role1->setName("Муж");
    role1->setDescription("Ты любишь жену, но в последнее время вы отдалились друг от друга, и жизнь стала серой и безвкусной. А ее лучшая подруга нравится тебе все больше и больше. Ты оказываешь ей знаки внимания, и кажется, что она отвечает взаимностью. Это вдохновляет тебя. Ты готов к любым авантюрам.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Adult}));
    role2->setName("Женщина");
    role2->setDescription("Его жена — твоя лучшая подруга со школы. И ей всегда доставалось лучшее: дом, внешность. Мужчины. Ты в тайне ненавидишь ее и уже давно влюблена в ее мужа. Ты зацепишься за любой повод, чтобы заполучить его.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Female, Tag::Adult, Tag::Beast}));
    role3->setName("Жена");
    role3->setDescription("Вы дружите со школы. Вы как сестры. Ты во всем ее поддерживаешь и всегда находишь ей оправдание.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Child}));
    role4->setName("Первый ребенок");
    role4->setDescription("Твой отец в последнее время сам не свой. Кажется, он изменяет матери. Надо разобраться в этом.");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Child}));
    role5->setName("Второй ребенок");
    role5->setDescription("Твоя мама в последнее время сама не своя. Кажется, у нее проблемы с отцом. Надо разобраться в этом.");
    relationship->addRole(role5);

    relationship->addRoleGroup(Roles({role1, role3, role4, role5}));
    relationship->addRoleGroup(Roles({role2}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role2}));
    relationship->addRelation(Roles({role4, role1}));
    relationship->addRelation(Roles({role5, role3}));

    return relationship;
}

Relationship* RelationshipFactory::getYoungFemalesVengence()
{
    Relationship* relationship = new Relationship("Месть юные женщины");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    role1->setName("Персонаж 1");
    role1->setDescription("Она считает себя твоей подругой и постоянно \"заботится\" о тебе. Например, разболтала твоим родителям, что ты пробовала наркотики. Такое предательство простить нельзя. Ты ищешь того, кто может тебе помочь отомстить.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    role2->setName("Персонаж 2");
    role2->setDescription("Твоя подруга катится по наклонной, и ее надо спасать. Ты надеялась, что родители помогут ей преодолеть зависимость от наркотиков. Ты рассказала им, и с тех пор она тебе не доверяет. Ты хочешь вернуть доверие, потому что дружба - это очень важно.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Child}));
    role3->setName("Персонаж 3");
    role3->setDescription("Дружба мужчины и женщины существует. Она твой лучший друг - за нее порвешь любого. Ей нужно помочь.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role4->setName("Персонаж 4");
    role4->setDescription("Твоя дочь употребляет наркотики. Тебе рассказала об этом ее подруга. ");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role5->setName("Персонаж 5");
    role5->setDescription("Тебе кажется, что если кто и способен повлиять на дочь, то только ее подруга.");
    relationship->addRole(role5);

    relationship->addRoleGroup(Roles({role1, role4}));
    relationship->addRoleGroup(Roles({role2, role5}));
    relationship->addRoleGroup(Roles({role3}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role4, role1}));
    relationship->addRelation(Roles({role5, role2}));

    return relationship;
}

Relationship* RelationshipFactory::getAdoptedSon()
{
    Relationship* relationship = new Relationship("Усыновленный сын");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Child}));
    role1->setName("Мальчик 1");
    role1->setDescription("Вчера мир для тебя перевернулся. Ты узнал, что твои родители - приемные. Ты еще никому не говорил. Ты чувствуешь себя обманутым, и первое твое желание - уйти из дома. Поддержит ли тебя брат?");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Child}));
    role2->setName("Мальчик 2");
    role2->setDescription("Ты гордишься своей семьей вопреки всему. Последнее время ты замечаешь, что брата что-то тревожит. Он вспыльчивый и может наломать дров.  ");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role3->setName("Взрослый 1");
    role3->setDescription("Твои дети - приемные. Ты сделаешь все, чтобы они были счастливы и никогда об этом не узнали.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Child}));
    role4->setName("Ребенок");
    role4->setDescription("Последнее время ты замечаешь, что твои братья и твои родители что-то от тебя скрывают.");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role5->setName("Взрослый 2");
    role5->setDescription("Твои дети - приемные. Ты сделаешь все, чтобы они были счастливы и никогда об этом не узнали.");
    relationship->addRole(role5);

    relationship->addRoleGroup(Roles({role1, role2, role3, role4, role5}));

    relationship->addRelation(Roles({role1, role3}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role2, role5}));
    relationship->addRelation(Roles({role5, role2}));
    relationship->addRelation(Roles({role4, role1}));

    return relationship;
}

Relationship* RelationshipFactory::getTwoSisters()
{
    Relationship* relationship = new Relationship("Две сестры");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    role1->setName("Девочка 1");
    role1->setDescription("Твою сестру всегда любили и опекали больше, чем тебя. Зато ты стала самостоятельной и сама определяешь свою судьбу. Ты учишься в университете, но в твоих планах найти работу и стать независимой от родителей.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    role2->setName("Девочка 2");
    role2->setDescription("Родители всегда опекали тебя больше, чем сестру, и у нее всегда было столько свободы! Ты хочешь быть похожей на нее: такой же смелой и независимой. Ведь ты уже выросла! Хватит относится к тебе, как к ребенку.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role3->setName("Взрослый 1");
    role3->setDescription("Твои дети такие разные. Одна самостоятельная, а другая избалованная принцесса.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role4->setName("Взрослый 2");
    role4->setDescription("Твои дети такие разные. Одна самостоятельная, а другая избалованная принцесса.");
    relationship->addRole(role4);

    relationship->addRoleGroup(Roles({role1, role2, role3, role4}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role4, role2}));

    return relationship;
}

//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
Relationship* RelationshipFactory::getAdoptedDaughter()
{
    Relationship* relationship = new Relationship("Усыновленная дочь");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    role1->setName("Персонаж 1");
    role1->setDescription("Мир перевернулся для тебя. Ты узнала, что родители усыновили вас. Но сестра еще не в курсе. Ты чувствуешь себя обманутой. И первое твое желание - уйти из дома. Поддержит ли тебя сестра?");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    role2->setName("Персонаж 2");
    role2->setDescription("Ты гордишься своей семьей вопреки всему. Последнее время ты замечаешь, что сестру что-то тревожит. Он импульсивная и может наломать дров.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role3->setName("Персонаж 3");
    role3->setDescription("Твои дети - приемные. Ты делаешь все, чтобы они были счастливы и никогда об этом не узнали.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Child}));
    role4->setName("Персонаж 4");
    role4->setDescription("Последнее время ты замечаешь, что твои родители что-то от тебя скрывают.");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role5->setName("Персонаж 5");
    role5->setDescription("Твои дети - приемные. Ты делаешь все, чтобы они были счастливы и никогда об этом не узнали.");
    relationship->addRole(role5);

    relationship->addRoleGroup(Roles({role1, role2, role3, role4, role5}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role4, role3}));
    relationship->addRelation(Roles({role5, role1}));

    return relationship;
}

Relationship* RelationshipFactory::getNewFire()
{
    Relationship* relationship = new Relationship("Новый огонь");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Adult}));
    role1->setName("Персонаж 1");
    role1->setDescription("Семья очень много значит для вас. Но не так давно вы поняли, что за работой и повседневностью незаметно стали чужими друг для друга. Все рушится и идет к разводу. И вы приняли решение неделю жить так, будто вы встретились только вчера. Впереди фестиваль, и ты готовишь ей сюрприз.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Adult}));
    role2->setName("Персонаж 2");
    role2->setDescription("Семья очень много значит для вас. Но не так давно вы поняли, что за работой и повседневностью незаметно стали чужими друг для друга. Все рушится и идет к разводу. И вы приняли решение неделю жить так, будто вы встретились только вчера. Впереди фестиваль, и ты готовишь ему сюрприз.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Female, Tag::Child}));
    role3->setName("Персонаж 3");
    role3->setDescription("Твоя мама в последнее время сама не своя. Кажется, у нее проблемы с отцом. Надо разобраться в этом.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Child}));
    role4->setName("Персонаж 4");
    role4->setDescription("Отец в последнее время сам не свой. Кажется, у него проблемы с мамой. Надо разобраться в этом.");
    relationship->addRole(role4);

    relationship->addRoleGroup(Roles({role1, role2, role3, role4}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role4, role2}));

    return relationship;
}

Relationship* RelationshipFactory::getBigGame()
{
    Relationship* relationship = new Relationship("Большая игра");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Adult}));
    role1->setName("Персонаж 1");
    role1->setDescription("Вы вместе с самого детства. Друг для друга вы - космос. Вы постоянно что-то затеваете, и каждый год перед фестивалем играете в Игру: утром придумываете друг другу задание. В прошлом году ты пел в мэрии, а она проехала по городу верхом на осле. Сегодня первый день Игры, и она ждет задание от тебя.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Adult}));
    role2->setName("Персонаж 2");
    role2->setDescription("Вы вместе с самого детства. Друг для друга вы - космос. Вы постоянно что-то затеваете, и каждый год перед фестивалем играете в Игру: утром придумываете друг другу задание. В прошлом году он пел в мэрии, а ты проехала по городу верхом на осле. Сегодня первый день Игры, и он ждет задание от тебя.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Adult}));
    role3->setName("Персонаж 3");
    role3->setDescription("Она клевая! Ты бы на такой женился) Ты часто зовешь ее выпить кофе или прогуляться после работы.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Female, Tag::Adult}));
    role4->setName("Персонаж 4");
    role4->setDescription("Он клевый! Жалко, что женат) Ты часто зовешь его выпить кофе или прогуляться после работы.");
    relationship->addRole(role4);

    relationship->addRoleGroup(Roles({role1, role2, role3, role4}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role2}));
    relationship->addRelation(Roles({role4, role1}));

    return relationship;
}

Relationship* RelationshipFactory::getFearOfLose()
{
    Relationship* relationship = new Relationship("Страх потерять");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Adult}));
    role1->setName("Персонаж 1");
    role1->setDescription("Отношения с женой давно остыли. Но ее новая работа и постоянные командировки изменили ее. Ты увидел ее по-другому и теперь боишься  потерять.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Adult}));
    role2->setName("Персонаж 2");
    role2->setDescription("Отношения с мужем давно остыли. Твоя новая работа и командировки изменили тебя и дали почву для ревности с его стороны. Сможешь ли ты воспользоваться этим, чтобы снова зажечь огонь?");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Adult}));
    role3->setName("Персонаж 3");
    role3->setDescription("Она клевая! Ты бы на такой женился) Ты часто зовешь ее выпить кофе или прогуляться после работы.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Child}));
    role4->setName("Персонаж 4");
    role4->setDescription("Твоя мама в последнее время сама не своя. Кажется, у нее проблемы с отцом. Надо разобраться в этом.");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Child}));
    role5->setName("Персонаж 5");
    role5->setDescription("Отец в последнее время сам не свой. Кажется, у него проблемы с мамой. Надо разобраться в этом.");
    relationship->addRole(role5);

    relationship->addRoleGroup(Roles({role1, role2, role4, role5}));
    relationship->addRoleGroup(Roles({role3}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role2}));
    relationship->addRelation(Roles({role4, role1}));
    relationship->addRelation(Roles({role5, role2}));

    return relationship;
}

Relationship* RelationshipFactory::getJealous()
{
    Relationship* relationship = new Relationship("Ревнивец");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Child}));
    role1->setName("Персонаж 1");
    role1->setDescription("Ты любишь ее больше, чем можно представить. А она любит тебя. Но она еще совсем наивная девочка, все время делает глупости... И ты контролируешь каждый ее шаг. Ради вашего общего будущего. ");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Child}));
    role2->setName("Персонаж 2");
    role2->setDescription("Ты любишь его больше неба и выше солнца. Он самый лучший, и другого быть не может. Но с каждым днем он все больше контролирует твою жизнь. Ты думаешь о том, что стены клетки вокруг тебя становятся теснее.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Child}));
    role3->setName("Персонаж 3");
    role3->setDescription("Ее парень тебя бесит. Пудрить мозги ей - лучший способ вывести его из себя. Она тебя не интересует.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role4->setName("Персонаж 4");
    role4->setDescription("Неудачник!  Пусть отстанет от твоей дочери! Он тебя бесит. Ты найдешь ей пару получше.");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role5->setName("Персонаж 5");
    role5->setDescription("Малолетняя шалава! Когда уже она оставит твоего сына в покое? Ты сделаешь все чтобы они расстались.");
    relationship->addRole(role5);

    relationship->addRoleGroup(Roles({role2, role4}));
    relationship->addRoleGroup(Roles({role1, role5}));
    relationship->addRoleGroup(Roles({role3}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role2}));
    relationship->addRelation(Roles({role4, role1}));
    relationship->addRelation(Roles({role5, role2}));

    return relationship;
}

Relationship* RelationshipFactory::getAdultAndPregnant()
{
    Relationship* relationship = new Relationship("Взрослый залет");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Female, Tag::Adult, Tag::Married}));
    role1->setName("Персонаж 1");
    role1->setDescription("Ты встретила невероятного человека и потеряла голову. Твой муж неплохой человек, и тебе дорога ваша семья. Ты легко держала измену в тайне. Но недавно узнала, что беременна, и не знаешь точно, кто отец ребенка. Ты тайно заказала тест на отцовство мужа. На днях ты получишь по почте конверт с результатом.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Male, Tag::Adult}));
    role2->setName("Персонаж 2");
    role2->setDescription("Ты встретил потрясающую женщину, и вы по-настоящему полюбили друг друга. Но у нее семья и влиятельный муж... Ты понимаешь, как это важно. В последнее время она явно чем-то озабочена. Может, она хочет разорвать ваши отношения? Нужно помочь ей разобраться. Ты больше не намерен делить ее с другим.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Male, Tag::Adult}));
    role3->setName("Персонаж 3");
    role3->setDescription("В последнее время твоя жена явно чем-то очень озабочена. Может быть, ей нужна твоя помощь?");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Child}));
    role4->setName("Персонаж 4");
    role4->setDescription("В последнее время твоя мама явно чем-то очень озабочена. Может быть, ей нужна твоя помощь? ");
    relationship->addRole(role4);

    relationship->addRoleGroup(Roles({role1, role3, role4}));
    relationship->addRoleGroup(Roles({role2}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role4, role1}));

    return relationship;
}

Relationship* RelationshipFactory::getFreaks1()
{
    Relationship* relationship = new Relationship("Неформалы");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Child, Tag::Student}));
    role1->setName("Персонаж 1");
    role1->setDescription("Вы - парочка неформалов, и в этом городе вы бунтуете против системы. Самый большой конфликт у вас с преподом в универе. Его придирки и ваши эпатажные выходки привели вас к состоянию войны. Финальной битвой станет фестиваль. Вы думаете, что сделать, чтобы он запомнил этот день на всю жизнь.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Child, Tag::Student}));
    role2->setName("Персонаж 2");
    role2->setDescription("Вы - парочка неформалов, и в этом городе вы бунтуете против системы. Самый большой конфликт у вас с преподом в универе. Его придирки и ваши эпатажные выходки привели вас к состоянию войны. Финальной битвой станет фестиваль. Вы думаете, что сделать, чтобы он запомнил этот день на всю жизнь.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role3->setName("Персонаж 3");
    role3->setDescription("Это главный фрик в твоем городе. Только дай ему волю и он потащит за собой твоих детей.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role4->setName("Персонаж 4");
    role4->setDescription("Твой ребенок - главный фрик в этом городе. Видно в детстве мало пороли! Пора взяться за него серьезно.");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role5->setName("Персонаж 5");
    role5->setDescription("Твой ребенок - фрик. Его выходки уже достали. Как, где и с кем он попадет в тюрьму тебе уже не важно.");
    relationship->addRole(role5);

    Role* role6 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role6->setName("Персонаж 6");
    role6->setDescription("Твой ребенок - фрик. Его выходки уже достали. Как, где и с кем он попадет в тюрьму тебе уже не важно.");
    relationship->addRole(role6);

    relationship->addRoleGroup(Roles({role1, role5, role6}));
    relationship->addRoleGroup(Roles({role2, role4}));
    relationship->addRoleGroup(Roles({role3}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role4, role2}));
    relationship->addRelation(Roles({role5, role1}));
    relationship->addRelation(Roles({role6, role1}));

    return relationship;
}

Relationship* RelationshipFactory::getFreaks2()
{
    Relationship* relationship = new Relationship("Неформалы");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Child, Tag::Student}));
    role1->setName("Персонаж 1");
    role1->setDescription("Вы - парочка неформалов, и в этом городе вы бунтуете против системы. Самый большой конфликт у вас с преподом в универе. Его придирки и ваши эпатажные выходки привели вас к состоянию войны. Финальной битвой станет фестиваль. Вы думаете, что сделать, чтобы он запомнил этот день на всю жизнь.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Child, Tag::Student}));
    role2->setName("Персонаж 2");
    role2->setDescription("Вы - парочка неформалов, и в этом городе вы бунтуете против системы. Самый большой конфликт у вас с преподом в универе. Его придирки и ваши эпатажные выходки привели вас к состоянию войны. Финальной битвой станет фестиваль. Вы думаете, что сделать, чтобы он запомнил этот день на всю жизнь.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role3->setName("Персонаж 3");
    role3->setDescription("Это главный фрик в твоем городе. Только дай ему волю и он потащит за собой твоих детей.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role4->setName("Персонаж 4");
    role4->setDescription("Твой ребенок - главный фрик в этом городе. Видно в детстве мало пороли! Пора взяться за него серьезно.");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role5->setName("Персонаж 5");
    role5->setDescription("Твой ребенок - фрик. Его выходки уже достали. Как, где и с кем он попадет в тюрьму тебе уже не важно.");
    relationship->addRole(role5);

    Role* role6 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role6->setName("Персонаж 6");
    role6->setDescription("Твой ребенок - фрик. Его выходки уже достали. Как, где и с кем он попадет в тюрьму тебе уже не важно.");
    relationship->addRole(role6);

    relationship->addRoleGroup(Roles({role1, role5, role6}));
    relationship->addRoleGroup(Roles({role2, role4}));
    relationship->addRoleGroup(Roles({role3}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role4, role2}));
    relationship->addRelation(Roles({role5, role1}));
    relationship->addRelation(Roles({role6, role1}));

    return relationship;
}

Relationship* RelationshipFactory::getBadVolunteer()
{
    Relationship* relationship = new Relationship("Плохой волонтер");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Volunteer, Tag::Child, Tag::Male}));
    role1->setName("Персонаж 1");
    role1->setDescription("Причиной твоего отъезда стала ваша ссора. А точнее - твоя измена. Это было так глупо! Вечеринка, выпивка... И, конечно, это трудно простить. Но ты теперь сильнее и мудрее. Ты веришь, что сумеешь восстановить отношения и никому не отдашь свою любовь.");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Child, Tag::Female}));
    role2->setName("Персонаж 2");
    role2->setDescription("Вы очень любили друг друга и собирались пожениться. Поэтому застукать их на вечеринке голыми в одной комнате было сильным ударом. Ты не из тех, кто может легко простить и забыть измену. И вот твоя любовь уходит в волонтеры и уезжает... А сегодня ночью возвращается в город. Может быть - к тебе?");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role3->setName("Персонаж 3");
    role3->setDescription("Твой ребенок ушел волонтером и вернулся. Ему нелегко, но он всегда может на тебя рассчитывать.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role4->setName("Персонаж 4");
    role4->setDescription("Твой ребенок ушел волонтером и вернулся. Ему нелегко, но он всегда может на тебя рассчитывать.");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Volunteer}));
    role5->setName("Персонаж 5");
    role5->setDescription("Вы вместе ушли в волонтеры, прошли через кошмар и вместе вернулись. И проблемы у вас тоже общие.");
    relationship->addRole(role5);

    relationship->addRoleGroup(Roles({role1, role3, role4}));
    relationship->addRoleGroup(Roles({role2}));
    relationship->addRoleGroup(Roles({role5}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role4, role1}));
    relationship->addRelation(Roles({role5, role1}));

    return relationship;
}

Relationship* RelationshipFactory::getGoodVolunteer()
{
    Relationship* relationship = new Relationship("Хороший волонтер");
    RoleFactory* rolesFactory = new RoleFactory(relationship);

    Role* role1 = rolesFactory->getFirstRole(Tags({Tag::Volunteer, Tag::Child, Tag::Female}));
    role1->setName("Персонаж 1");
    role1->setDescription("Ты не ищешь легких путей. Уйти волонтером, проявить себя, помогая людям, было очень важно для тебя. И любовь, оставленная здесь, помогла не сойти с ума в том аду. Каково же было узнать, что \"любовь\", пока тебя не было, зажигала с каждым встречным? Что может быть хуже? Может, это слухи? Кому верить?");
    relationship->addRole(role1);

    Role* role2 = rolesFactory->getFirstRole(Tags({Tag::Child, Tag::Male}));
    role2->setName("Персонаж 2");
    role2->setDescription("Было нелегко принять, что твоя вторая половина уходит волонтером. Но еще труднее вынести то, что после возвращения тебя обвиняют в измене, и друзья доносят обо всех, с кем тебя видели в парке или в кино. Как доказать свою невиновность? Стоит ли? Но ведь это твоя единственная любовь.");
    relationship->addRole(role2);

    Role* role3 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role3->setName("Персонаж 3");
    role3->setDescription("Твой ребенок ушел волонтером и вернулся. Ему нелегко, но он всегда может на тебя рассчитывать.");
    relationship->addRole(role3);

    Role* role4 = rolesFactory->getSecondRole(Tags({Tag::Adult}));
    role4->setName("Персонаж 4");
    role4->setDescription("Твой ребенок ушел волонтером и вернулся. Ему нелегко, но он всегда может на тебя рассчитывать.");
    relationship->addRole(role4);

    Role* role5 = rolesFactory->getSecondRole(Tags({Tag::Volunteer}));
    role5->setName("Персонаж 5");
    role5->setDescription("Вы вместе ушли в волонтеры, прошли через кошмар и вместе вернулись. И проблемы у вас тоже общие.");
    relationship->addRole(role5);

    Role* role6 = rolesFactory->getSecondRole(Tags({Tag::Child}));
    role6->setName("Персонаж 6");
    role6->setDescription("Ненавидишь выскочек. Волонтерство - большая глупость. Для тех, кто не хочет ни работать, ни учиться.");
    relationship->addRole(role6);

    relationship->addRoleGroup(Roles({role1, role3, role4}));
    relationship->addRoleGroup(Roles({role2}));
    relationship->addRoleGroup(Roles({role5}));
    relationship->addRoleGroup(Roles({role6}));

    relationship->addRelation(Roles({role1, role2}));
    relationship->addRelation(Roles({role2, role1}));
    relationship->addRelation(Roles({role3, role1}));
    relationship->addRelation(Roles({role4, role1}));
    relationship->addRelation(Roles({role5, role1}));
    relationship->addRelation(Roles({role6, role1}));

    return relationship;
}

Relationships RelationshipFactory::getRelationships()
{
    Relationships relationships;
    relationships.append(getYoungFemalesVengence());
    relationships.append(getFemaleAdultsEnvy());
    relationships.append(getRomeoAndJuliet());
    relationships.append(getRomeoAndJuliet());
    relationships.append(getBeautyAndTheBeast());
    relationships.append(getBeautyAndTheBeast());
    relationships.append(getOneginAndTatiana());
    relationships.append(getSnowWhite());
    relationships.append(getSnowWhite());
    relationships.append(getAdultBetrayal());
    relationships.append(getSnowWhiteReversed());
    relationships.append(getSnowWhiteReversed());
    relationships.append(getYoungAndPregnant());
    relationships.append(getYoungAndPregnant());
    relationships.append(getAdoptedSon());
    relationships.append(getTwoSisters());
    relationships.append(getTwoSisters());
    relationships.append(getAdoptedDaughter());
    relationships.append(getAdoptedDaughter());
    relationships.append(getNewFire());
    relationships.append(getBigGame());
    relationships.append(getFearOfLose());
    relationships.append(getJealous());
    relationships.append(getAdultAndPregnant());
    relationships.append(getFreaks1());
    relationships.append(getFreaks2());
    relationships.append(getBadVolunteer());
    relationships.append(getGoodVolunteer());
    addRelationshipIndexes(relationships);
    relationships = Helpers::shuffle(relationships);
    return relationships;
}

void RelationshipFactory::addRelationshipIndexes(Relationships relationships)
{
    QVector<QString> used;
    for (int i = 0; i < relationships.length(); i++)
    {
        if (!used.contains(relationships[i]->getTitle()))
        {
            relationships[i]->setIndex(i);
            used.append(relationships[i]->getTitle());
        }
    }
}
