#include "familyfactory.h"

FamilyFactory::FamilyFactory(QObject* parent) : QObject(parent) {}

Family* FamilyFactory::getRedFamily(QObject* parent)
{
    Family* family = new Family(FamilyTag::Red, parent);
    family->setFamilyName("Рэд");
    return family;
}

Family* FamilyFactory::getBlackFamily(QObject* parent)
{
    Family* family = new Family(FamilyTag::Black, parent);
    family->setFamilyName("Блэк");
    return family;
}

Family* FamilyFactory::getLiFamily(QObject* parent)
{
    Family* family = new Family(FamilyTag::Li, parent);
    family->setFamilyName("Ли");
    return family;
}

Family* FamilyFactory::getOstinFamily(QObject* parent)
{
    Family* family = new Family(FamilyTag::Ostin, parent);
    family->setFamilyName("Остин");
    return family;
}

Family* FamilyFactory::getWoodFamily(QObject* parent)
{
    Family* family = new Family(FamilyTag::Wood, parent);
    family->setFamilyName("Вуд");
    return family;
}

Family* FamilyFactory::getSmitFamily(QObject* parent)
{
    Family* family = new Family(FamilyTag::Smith, parent);
    family->setFamilyName("Смит");
    return family;
}

Families FamilyFactory::getFamilies(QObject* parent)
{
    Families families;
    families.append(getSmitFamily(parent));
    families.append(getRedFamily(parent));
    families.append(getBlackFamily(parent));
    families.append(getLiFamily(parent));
    families.append(getWoodFamily(parent));
    families.append(getOstinFamily(parent));
    return families;
}
