#include "member.h"

Member::Member(Family* family, Tags tags, QObject *parent)
    : QObject(parent)
{
    this->family = family;
    this->tags = tags;
}

QString Member::getName() { return name; }
QString Member::getFamilyStatus() { return familyStatus; }
QString Member::getLivingPlace() { return livingPlace; }
QString Member::getMorningRoutine() { return morningRoutine; }
QString Member::getDayRoutine() { return dayRoutine; }
QString Member::getEveningRoutine() { return eveningRoutine; }
QString Member::getFamilyDescription() { return familyDescription; }
Family* Member::getFamily() { return family; }
Roles Member::getRoles() { return roles; }
QString Member::getOrganization() { return organization; }
QString Member::getOrganizationDescription() { return organizationDescription; }
QString Member::getOrganizationPosition() { return organizationPosition; }
Tags Member::getTags() { return tags; }
QHash<QString, QString> Member::getRelationsInfo() { return relationsInfo; }
int Member::getOrganizationIndex() { return this->organizationIndex; }

Tag Member::getGender()
{
    if (tags.contains(Tag::Male)) return Tag::Male;
    else if (tags.contains(Tag::Female)) return Tag::Female;
    return Tag::None;
}

Tag Member::getMaturity()
{
    if (tags.contains(Tag::Child)) return Tag::Child;
    else if (tags.contains(Tag::Adult)) return Tag::Adult;
    return Tag::None;
}

void Member::setName(QString name) { this->name = name; }
void Member::setFamilyStatus(QString familyStatus) { this->familyStatus = familyStatus; }
void Member::setLivingPlace(QString livingPlace) { this->livingPlace = livingPlace; }
void Member::setMorningRoutine(QString morningRoutine) { this->morningRoutine = morningRoutine; }
void Member::setDayRoutine(QString dayRoutine) { this->dayRoutine = dayRoutine; }
void Member::setEveningRoutine(QString eveningRoutine) { this->eveningRoutine = eveningRoutine; }
void Member::setFamilyDescription(QString familyDescription) { this->familyDescription = familyDescription; }
void Member::setFamily(Family* family) { this->family = family; }
void Member::setOrganization(QString organization) { this->organization = organization; }
void Member::setOrganizationDescription(QString description) { this->organizationDescription = description; }
void Member::setOrganizationPosition(QString position) { this->organizationPosition = position; }
void Member::setRoles(Roles roles) { this->roles = roles; }
void Member::setRelationsInfo(QHash<QString, QString> relationsInfo) { this->relationsInfo = relationsInfo; }
void Member::setOrganizationIndex(int index) { this->organizationIndex = index; }
void Member::addTag(Tag tag) { tags.append(tag); }
void Member::addRole(Role* role) { roles.append(role); }
void Member::addRelationInfo(QString name, QString description) { relationsInfo.insert(name, description); }
