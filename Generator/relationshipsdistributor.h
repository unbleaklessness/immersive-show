#ifndef RELATIONSHIPSDISTRIBUTOR_H
#define RELATIONSHIPSDISTRIBUTOR_H

#include <QObject>

#include "relationship.h"
#include "roleshandler.h"
#include "family.h"

class RelationshipsDistributor : public QObject
{
    Q_OBJECT

private:
    static void addRoleGroupToFamily(Roles roles, Family* family);
    static bool isRelationshipInFamily(Relationship* relationship, Family* family);
    static int nRolesFitsInFamily(Roles roles, Family* family);
    static int iBestFamilyForRoleGroup(Roles roles, Families families);
    static bool isMemberFitsInRole(Member* member, Role* role);
    static void distributeRoleGroup(Roles roles, Families families);
    static bool isRelationshipIncomplete(Relationship* relationship, Families families);
    static void removeRelationship(Relationship* relationship, Families families);
    static void removeRoles(Families families);
    static int countMembersWithMaxNRoles(Families families, int n);
    static int countMembersWithMinNRoles(Families families, int n);
    static void distributeRoleGroups(Relationships relationships, Families families);
    static void addRelatedMembers(Relationships relationship, Families families);
    static Members findRoleInFamilies(Role* role, Families families);

public:
    static void distribute(Relationships relationships, Families families);
};

#endif // RELATIONSHIPSDISTRIBUTOR_H
