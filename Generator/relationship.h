#ifndef RELATIONSHIP_H
#define RELATIONSHIP_H

#include <QObject>

#include "role.h"
#include "tag.h"

enum class Tag;
class Role;
class Family;
typedef QVector<Role*> Roles;
typedef QVector<Roles> RoleGroups;

class Relationship : public QObject
{
    Q_OBJECT

private:
    QString title;
    Tags tags;
    Roles roles;
    RoleGroups roleGroups;
    RoleGroups relations;
    int index;

public:
    explicit Relationship(QString title, QObject* parent = nullptr);

    QString getTitle();
    Tags getTags();
    RoleGroups getRoleGroups();
    RoleGroups getRelations();
    int getIndex();

    void setTitle(QString title);
    void setTags(Tags tags);
    void setRelations(RoleGroups relations);
    void setRoleGroups(RoleGroups roleGroups);
    void addRole(Role* role);
    void addRoleGroup(Roles group);
    void addRelation(Roles relation);
    void setIndex(int index);
};

typedef QVector<Relationship*> Relationships;

#endif // RELATIONSHIP_H
