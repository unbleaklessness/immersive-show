#ifndef ROLESFACTORY_H
#define ROLESFACTORY_H

#include <QObject>

#include "role.h"
#include "tag.h"
#include "relationship.h"

class Relationship;
class Role;

class RoleFactory : public QObject
{
    Q_OBJECT

private:
    Relationship* relationship;

public:
    explicit RoleFactory(Relationship* relationship);

    Role* getFirstRole(Tags tags);
    Role* getSecondRole(Tags tags);
};

#endif // ROLESFACTORY_H
