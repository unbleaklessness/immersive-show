#include "relationship.h"

Relationship::Relationship(QString title, QObject* parent)
    : QObject(parent)
{
    this->title = title;
}

Tags Relationship::getTags() { return tags; }
QString Relationship::getTitle() { return title; }
RoleGroups Relationship::getRoleGroups() { return roleGroups; }
RoleGroups Relationship::getRelations() { return relations; }
int Relationship::getIndex() { return this->index; }

void Relationship::setTitle(QString title) { this->title = title; }
void Relationship::setTags(Tags tags) { this->tags = tags; }
void Relationship::setRelations(RoleGroups relations) { this->relations = relations; }
void Relationship::setRoleGroups(RoleGroups roleGroups) { this->roleGroups = roleGroups; }
void Relationship::addRoleGroup(Roles group) { roleGroups.append(group); }
void Relationship::addRole(Role* role) { roles.append(role); }
void Relationship::addRelation(Roles relation) { relations.append(relation); }
void Relationship::setIndex(int index) { this->index = index; }
