#ifndef FAMILIES_FACTORY_H
#define FAMILIES_FACTORY_H

#include <QObject>
#include <QVector>

#include "family.h"

class FamilyFactory : public QObject
{
    Q_OBJECT

public:
    explicit FamilyFactory(QObject* parent = nullptr);

    Family* getRedFamily(QObject* parent = nullptr);
    Family* getBlackFamily(QObject* parent = nullptr);
    Family* getLiFamily(QObject* parent = nullptr);
    Family* getOstinFamily(QObject* parent = nullptr);
    Family* getWoodFamily(QObject* parent = nullptr);
    Family* getSmitFamily(QObject* parent = nullptr);

    Families getFamilies(QObject* parent = nullptr);
};

#endif // FAMILIES_FACTORY_H
