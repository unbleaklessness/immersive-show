#ifndef RELATIONSHIP_MEMBER_H
#define RELATIONSHIP_MEMBER_H

#include <QObject>

#include "relationship.h"
#include "roletype.h"
#include "tag.h"

enum class Tag;
class Relationship;

class Role : public QObject
{
    Q_OBJECT

private:
    QString title;
    QString description;
    QString name;
    Tags tags;
    Relationship* relationship;
    RoleType roleType;
    Role* relation;

public:
    explicit Role(Tags tags, Relationship* relationship, QObject* parent = nullptr);

    Tags getTags();
    QString getDescription();
    QString getName();
    Relationship* getRelationship();
    RoleType getRoleType();
    QString getTitle();
    Role* getRelation();

    void setRelationship(Relationship* relationship);
    void setRoleType(RoleType roleType);
    void setTitle(QString title);
    void setDescription(QString description);
    void setName(QString name);
    void setRelation(Role* role);
};

typedef QVector<Role*> Roles;
typedef QVector<Roles> RoleGroups;

#endif // RELATIONSHIP_MEMBER_H
