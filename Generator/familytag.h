#ifndef FAMILYTAG_H
#define FAMILYTAG_H

enum class FamilyTag
{
    Red, Wood, Ostin, Black, Smith, Li
};

#endif // FAMILYTAG_H
