#include "roleshandler.h"

int RolesHandler::nRoleType(Roles roles, RoleType roleType)
{
    int counter = 0;
    for (int i = 0; i < roles.length(); i++)
        if (roles[i]->getRoleType() == roleType)
            counter++;
    return counter;
}
