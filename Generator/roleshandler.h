#ifndef ROLESHANDLER_H
#define ROLESHANDLER_H

#include <QObject>

#include "roletype.h"
#include "role.h"

class RolesHandler : public QObject
{
    Q_OBJECT

public:
    static int nRoleType(Roles roles, RoleType roleType);
};

#endif // ROLESHANDLER_H
