#include "buttontoppanel.h"

ButtonTopPanel::ButtonTopPanel()
{
    setFixedSize(30,30);


    normalStyle = "width: 30px; height: 30px; border-radius: 15px; background-color: cyan;";
    pressedStyle = "width: 30px; height: 30px; border-radius: 15px; background-color: black;";
    setStyleSheet(normalStyle);


    connect(this, SIGNAL(pressed()), this, SLOT(setPressedSTL()));
    connect(this, SIGNAL(released()), this, SLOT(setNormalSTL()));


}

ButtonTopPanel::~ButtonTopPanel()
{

}

void ButtonTopPanel::setNormalStyle(QString style){
    normalStyle = style;
}

void ButtonTopPanel::setPressedStyle(QString style){
    pressedStyle = style;
}


// SLOTS
void ButtonTopPanel::setPressedSTL(){
    setStyleSheet(pressedStyle);
}

void ButtonTopPanel::setNormalSTL(){
    setStyleSheet(normalStyle);

}

