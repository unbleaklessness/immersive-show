#ifndef BUTTONTOPPANEL_H
#define BUTTONTOPPANEL_H

#include <QObject>
#include <QPushButton>
#include <QString>



class ButtonTopPanel : public QPushButton
{
Q_OBJECT
private:
    QString normalStyle;
    QString pressedStyle;
public:
    ButtonTopPanel();
    ~ButtonTopPanel();


    void setNormalStyle(QString style);
    void setPressedStyle(QString style);
public slots:
    void setPressedSTL();
    void setNormalSTL();




};

#endif // BUTTONTOPPANEL_H
