#include "buttonwithlabel.h"

ButtonWithLabel::ButtonWithLabel(QString name)
{
    setFixedHeight(75);


    rootLayout = new QVBoxLayout;
    rootLayout->setAlignment(Qt::AlignCenter);

    button = new ButtonTopPanel;
    label = new QLabel(name);


    label->setStyleSheet("color:black; font-size:11px");

    rootLayout->addWidget(button);
    rootLayout->addWidget(label);

    setLayout(rootLayout);
}
