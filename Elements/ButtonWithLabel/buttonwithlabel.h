#ifndef BUTTONWITHLABEL_H
#define BUTTONWITHLABEL_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>

#include "Elements/Button/buttontoppanel.h"


class ButtonWithLabel : public QWidget
{
    Q_OBJECT
private:
    QVBoxLayout *rootLayout;
public:
    ButtonWithLabel(QString name);

    ButtonTopPanel *button;
    QLabel *label;

};

#endif // BUTTONWITHLABEL_H
