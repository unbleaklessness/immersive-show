#ifndef TOPPANEL_H
#define TOPPANEL_H

#include <QWidget>
#include <QHBoxLayout>

#include "Elements/Button/buttontoppanel.h"
#include "Elements/ButtonWithLabel/buttonwithlabel.h"


class TopPanel : public QWidget
{
    Q_OBJECT
private:
    QHBoxLayout *rootLayout;
public:
    TopPanel();

    // BUTTONS
    ButtonWithLabel *generateRoles;
    ButtonWithLabel *printButton;
};

#endif // TOPPANEL_H
