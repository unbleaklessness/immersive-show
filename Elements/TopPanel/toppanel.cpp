#include "toppanel.h"

TopPanel::TopPanel()
{
    setMaximumHeight(80);

    rootLayout = new QHBoxLayout;
    rootLayout->setAlignment(Qt::AlignCenter | Qt::AlignLeft);

    generateRoles = new ButtonWithLabel("Сгенерировать роли");
    generateRoles->button->setNormalStyle("width: 30px; height: 30px; border-radius: 15px;background-color: #FF1A1A");
    generateRoles->button->setPressedStyle("width: 30px; height: 30px; border-radius: 15px;background-color: #B01717");
    generateRoles->button->setNormalSTL();
    rootLayout->addWidget(generateRoles);

    printButton = new ButtonWithLabel("Печать");
    printButton->button->setNormalStyle("width: 30px; height: 30px; border-radius: 15px;background-color: #59FB1B");
    printButton->button->setPressedStyle("width: 30px; height: 30px; border-radius: 15px;background-color: #356C20");
    printButton->button->setNormalSTL();
    rootLayout->addWidget(printButton);


    setLayout(rootLayout);
}
