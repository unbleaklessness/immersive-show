#include "convertfamilies.h"

ConvertFamilies::ConvertFamilies()
{

}

void ConvertFamilies::setFamilies(Families families)
{
    humanVector.clear();
    for (int i = 0; i < families.length(); i++)
    {
        Members members = families[i]->getMembers();
        for (int j = 0; j < members.length(); j++)
        {
            human.familyMembersNames.append(members[j]->getName());
        }
        parseFamily(families[i]);
        human.familyMembersNames.clear();
    }

}

void ConvertFamilies::parseFamily(Family* family)
{
    Members members = family->getMembers();
    for (int i = 0; i < members.length(); i++)
    {
        parseMember(members[i]);
        human.relationsInfo.clear();
    }
}

void ConvertFamilies::parseMember(Member* member)
{
    // First column
    human.name = member->getName();
    human.familyStatus = member->getFamilyStatus();
    human.work = member->getOrganizationPosition();
    human.location = member->getLivingPlace();
    human.relationship = member->getFamilyDescription();

    // Second Column
    human.relationsInfo = member->getRelationsInfo();

    // Third Column
    human.organizationDescription = member->getOrganizationDescription();
    human.morning = member->getMorningRoutine();
    human.day = member->getDayRoutine();
    human.evening = member->getEveningRoutine();

    // Index
    Tag gender = member->getGender();
    if (gender == Tag::Female) human.sexIndex = 0;
    else if (gender == Tag::Male) human.sexIndex = 1;
    Roles roles = member->getRoles();
    for (int i = 0; i < roles.length(); i++)
    {
        if (roles[i]->getRoleType() == RoleType::FirstRole)
            human.relationshipIndex = roles[i]->getRelationship()->getIndex();
        else human.relationshipIndex = -1;
    }
    human.organizationIndex = member->getOrganizationIndex();

    // Add human to vector
    humanVector.append(human);
}

QVector<Human> ConvertFamilies::getConvertedMembers()
{
    return humanVector;
}
