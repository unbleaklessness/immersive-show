#-------------------------------------------------
#
# Project created by QtCreator 2018-04-25T21:44:33
#
#-------------------------------------------------

QT       += core gui
QT       += printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = immersive-show
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    page.cpp \
    printer.cpp \
    convertfamilies.cpp \
    Generator/familieshandler.cpp \
    Generator/family.cpp \
    Generator/familyfactory.cpp \
    Generator/helpers.cpp \
    Generator/member.cpp \
    Generator/membershandler.cpp \
    Generator/relationship.cpp \
    Generator/relationshipfactory.cpp \
    Generator/relationshipsdistributor.cpp \
    Generator/role.cpp \
    Generator/rolefactory.cpp \
    Generator/roleshandler.cpp \
    Elements/Button/buttontoppanel.cpp \
    Elements/TopPanel/toppanel.cpp \
    Elements/ButtonWithLabel/buttonwithlabel.cpp \
    Window/GenerateDialog/generatedialog.cpp

HEADERS += \
        mainwindow.h \
    page.h \
    human.h \
    printer.h \
    convertfamilies.h \
    Generator/familieshandler.h \
    Generator/family.h \
    Generator/familyfactory.h \
    Generator/familytag.h \
    Generator/helpers.h \
    Generator/member.h \
    Generator/membershandler.h \
    Generator/relationship.h \
    Generator/relationshipfactory.h \
    Generator/relationshipsdistributor.h \
    Generator/role.h \
    Generator/rolefactory.h \
    Generator/roleshandler.h \
    Generator/roletype.h \
    Generator/tag.h \
    Elements/Button/buttontoppanel.h \
    Elements/TopPanel/toppanel.h \
    Elements/ButtonWithLabel/buttonwithlabel.h \
    Window/GenerateDialog/generatedialog.h

RESOURCES += \
    resources.qrc

